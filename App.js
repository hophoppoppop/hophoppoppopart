/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React,{Component} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Modal, BackHandler
} from 'react-native';

import Router from "./src/Routers/routerManager"
import {ImageViewer,ConfirmModal,PickerModal,LoadingModal,DatePickerModal} from "./src/Module";
import {onBackButtonPressAndroid} from "./src/Function/globalFunc";

export default class App extends Component{

    componentDidMount() {
        Text.defaultProps = Text.defaultProps || {};
        Text.defaultProps.allowFontScaling = false;
        BackHandler.addEventListener(
            'hardwareBackPress',
            onBackButtonPressAndroid
        );
    }

    render(){
        return (
            <View style={{flex:1,backgroundColor:"white"}}>
                <Router/>
                <ImageViewer/>
                <ConfirmModal/>
                <PickerModal/>
                <DatePickerModal />
                <LoadingModal/>
            </View>
        );
    }
}

