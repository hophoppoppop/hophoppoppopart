import React,{Component} from 'react';
import {View,TouchableOpacity} from 'react-native';
import COLORS from '../Constant/Colors';
import {Icon} from './index';
import PropTypes  from 'prop-types';

export default class CheckBox extends Component{

    static defaultProps = {
        size: 20,
        onValueChanged: (toggle)=>{

        },
        style: {

        },
        active: false,
    };

    static propTypes = {
        size: PropTypes.number,
        onValueChanged: PropTypes.any,
        style: PropTypes.any,
        active: PropTypes.any
    };

    constructor(props){
        super(props);
        this.state={
            active: false,
        };
    }
    
    render(){
        return (
            <TouchableOpacity
                style={[{width:this.props.size,height:this.props.size,borderRadius: 3,borderWidth:1,borderColor:COLORS.GREY,alignItems:"center",justifyContent:"center"},this.props.style]}
                onPress={()=>{
                    this.props.onValueChanged();
                }}
            >
                {this.props.active?<Icon size={this.props.size-8} name={"check"}/>:null}
            </TouchableOpacity>
        )
    }
}