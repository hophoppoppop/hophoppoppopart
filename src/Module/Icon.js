import React,{Component} from 'react';
import COLORS from '../Constant/Colors';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import PropTypes  from 'prop-types';

export default class Icon extends Component{

    /*
        type enum (fontAwesome, material, materialCommunity)
    */

    static defaultProps = {
        name: "",
        size: 12,
        color: COLORS.BLACK,
        type: "fontAwesome"
    };

    static propTypes = {
        name: PropTypes.any,
        size: PropTypes.any,
        color: PropTypes.any,
        type: PropTypes.any
    };

    constructor(props){
        super(props);
    }
    
    render(){
        if(this.props.type === "fontAwesome")
            return <FontAwesomeIcons  {...this.props}/>
        else if(this.props.type === "material")
            return <MaterialIcons {...this.props}/>
        else if(this.props.type === "materialCommunity")
            return <MaterialCommunityIcons {...this.props}/>
        else
            return null;
    }
}