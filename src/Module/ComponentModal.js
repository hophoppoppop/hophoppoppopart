
import React, {Component} from 'react';
import {
    View,
    Modal,
    TouchableOpacity,
    Text,
    TouchableWithoutFeedback,
    ScrollView
} from 'react-native';
import {observer} from 'mobx-react';
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import {Image,Icon} from "./index";

export default class ComponentModal extends Component{

    constructor(props){
        super(props);
        this.state = {
            visible: false
        }
    }

    toggleModal(toggle){
        this.setState({
            visible: toggle
        })
    }

    closeModal(){
        this.toggleModal(false);
    }

    render(){
        return (
            <Modal
                transparent
                onRequestClose={() => {
                    this.closeModal();
                }}
                visible={this.state.visible}>
                <TouchableOpacity onPress={()=>{
                    this.closeModal();
                }} style={{flex:1,backgroundColor: 'rgba(52, 52, 52, 0.8)',alignItems:"center",justifyContent:"flex-end"}}>
                    <TouchableWithoutFeedback>
                        <View
                            style={{width:"100%",backgroundColor:COLORS.WHITE,borderTopLeftRadius:20,borderTopRightRadius:20}}
                        >
                            <ScrollView>
                                {this.props.children}
                            </ScrollView>
                        </View>
                    </TouchableWithoutFeedback>
                </TouchableOpacity>
            </Modal>
        )
    }
}