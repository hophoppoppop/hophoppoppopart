
import React, {Component} from 'react';
import {
    View,
    Modal,
    TouchableOpacity,
    Text,
    TouchableWithoutFeedback
} from 'react-native';
import {observer} from 'mobx-react';
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import {Image,Icon} from "./index";

@observer

export default class PickerModal extends Component{

    constructor(props){
        super(props);
    }

    static togglePicker(title,data,pickAction){
        modalStore.pickerModal = {
            visible : !modalStore.pickerModal.visible,
            title: title,
            data: data,
            pickAction: (item)=>{
                modalStore.pickerModal.visible = false;
                pickAction(item);
            }
        };
    }

    closePickerModal(){
        modalStore.pickerModal.visible = false;
    }

    render(){
        return (
            <Modal
                transparent
                visible={modalStore.pickerModal.visible}
                onRequestClose={() => {
                    this.closePickerModal();
                }}>
                <TouchableOpacity onPress={()=>{
                    this.closePickerModal();
                }} style={{flex:1,backgroundColor: 'rgba(52, 52, 52, 0.8)',alignItems:"center",justifyContent:"flex-end"}}>
                    <TouchableWithoutFeedback>
                        <View style={{width:"100%",backgroundColor:COLORS.WHITE,borderTopLeftRadius:20,borderTopRightRadius:20}}>
                            <Text style={{fontSize:20,marginVertical:16,textAlign:"center",fontWeight:"bold"}}>
                                {modalStore.pickerModal.title}
                            </Text>
                            {
                                modalStore.pickerModal.data.map((item,index)=>
                                    <TouchableOpacity key={index} style={{width:"100%",paddingVertical:10,borderTopWidth:0.5,borderColor:COLORS.GREY,alignItems:"center"}} onPress={()=>{
                                        modalStore.pickerModal.pickAction(item);
                                    }}>
                                        <Text>
                                            {item.name}
                                        </Text>
                                    </TouchableOpacity>
                                )
                            }
                        </View>
                    </TouchableWithoutFeedback>
                </TouchableOpacity>
            </Modal>
        )
    }
}