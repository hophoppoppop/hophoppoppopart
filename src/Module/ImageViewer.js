
import React, {Component} from 'react';
import {
    View,
    Modal,
    TouchableOpacity
} from 'react-native';
import {observer} from 'mobx-react';
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import {Image,Icon} from "./index";

@observer
export default class ImageViewer extends Component{

    constructor(props){
        super(props);
    }

    static toggleImage(url){
        modalStore.imageViewer = {
            visible : !modalStore.imageViewer.visible,
            url : url?url:null,
        };
    }

    closeImageModal(){
        modalStore.imageViewer.visible = false;
    }

    render(){
        return (
            <Modal
                transparent
                visible={modalStore.imageViewer.visible}
                onRequestClose={() => {
                    this.closeImageModal();
                }}>
                <TouchableOpacity onPress={()=>{
                    this.closeImageModal();
                }} style={{flex:1,backgroundColor: 'rgba(52, 52, 52, 0.8)',alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:"100%",aspectRatio:1,alignItems:"center",justifyContent:"center",backgroundColor:COLORS.WHITE}}>
                        <Image style={{width:"100%",height:"100%",resizeMode:"contain"}} source={{uri:modalStore.imageViewer.url}}/>
                    </View>
                    <TouchableOpacity style={{position:"absolute",left:10,top:10}} onPress={()=>{
                        this.closeImageModal();
                    }}>
                        <Icon size={24} name={'times'} color={COLORS.WHITE}/>
                    </TouchableOpacity>
                </TouchableOpacity>
            </Modal>
        )
    }
}