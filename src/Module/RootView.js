
import React, {Component} from 'react';
import {
    View,
    StatusBar,
    SafeAreaView
} from 'react-native';
import {observer} from 'mobx-react';
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import Icon from 'react-native-vector-icons/FontAwesome';
import {Image} from "./index";
import PropTypes  from 'prop-types';

@observer
export default class RootView extends Component{

    static defaultProps = {
        barStyle: "dark-content",
        barColor: COLORS.WHITE,
    };

    static propTypes = {
        barStyle:PropTypes.any,
        barColor: PropTypes.any
    };

    constructor(props){
        super(props);
    }

    render(){
        return (
            <SafeAreaView style={{flex:1}}>
                <StatusBar animated backgroundColor={this.props.barColor} barStyle={this.props.barStyle} />
                {this.props.children}
            </SafeAreaView>
        )
    }
}