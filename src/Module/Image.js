import React,{Component} from 'react';
import FastImage from 'react-native-fast-image';

export default class Image extends Component{
    constructor(props){
        super(props);
    }
    
    render(){
        return <FastImage {...this.props} resizeMode={this.props.style.resizeMode?this.props.style.resizeMode:null}/>
    }
}