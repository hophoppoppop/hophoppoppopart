
import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import {observer} from 'mobx-react';
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import {Icon,Image} from "./index";
import PropTypes  from 'prop-types';
import {GetCurrentScene, PagePop} from "../Routers/routerManager";
import backButtonStore from "../Stores/backButtonStore";
import {onBackButtonPressAndroid} from "../Function/globalFunc";

@observer
export default class Header extends Component{

    static defaultProps = {
        title: "",
        backAction: ()=>{
            PagePop();
        },
        headerColor: COLORS.WHITE,
        backButtonColor: COLORS.BLACK,
        backButtonIcon: "chevron-left",
        noBorder: false,
        additionalAction: null,
        additionalButtonIcon: "check",
        additionalButtonColor: COLORS.GREEN
    };

    static propTypes = {
        title: PropTypes.any,
        backAction: PropTypes.any,
        headerColor: PropTypes.any,
        backButtonColor: PropTypes.any,
        backButtonIcon: PropTypes.any,
        noBorder: PropTypes.bool,
        additionalAction: PropTypes.any,
        additionalButtonIcon: PropTypes.any,
        additionalButtonColor: PropTypes.any
    };

    constructor(props){
        super(props);
    }

    componentDidMount(){
        if(backButtonStore.backButton.routerStack.length > 0)
        {
            backButtonStore.backButton.routerStack[backButtonStore.backButton.routerStack.length-1].action = this.props.backAction;
        }
    }

    render(){
        return (
            <View style={{width:"100%",height:60,backgroundColor: this.props.headerColor,flexDirection:"row",borderBottomWidth:this.props.noBorder?0:0.5,borderColor:COLORS.GREY}}>
                <TouchableOpacity style={{width:60,height:60,justifyContent:"center",alignItems:"center"}} onPress={()=>{
                    onBackButtonPressAndroid();
                }}>
                    <Icon name={this.props.backButtonIcon} size={25} color={this.props.backButtonColor}/>
                </TouchableOpacity>
                <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                    <Text style={{color: this.props.backButtonColor,fontSize:16,fontWeight: "bold"}}>{this.props.title}</Text>
                </View>
                {this.props.additionalAction !== null ?
                    <TouchableOpacity style={{width: 60, height: 60, justifyContent: "center", alignItems: "center"}}
                                      onPress={() => {
                                          this.props.additionalAction();
                                      }}>
                        <Icon name={this.props.additionalButtonIcon} size={25} color={this.props.additionalButtonColor}/>
            </TouchableOpacity> : <View style={{width: 60}}/>}
            </View>
        )
    }
}