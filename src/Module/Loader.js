import React,{Component} from 'react';
import {View} from 'react-native';
import SkeletonLoader  from 'react-native-skeleton-loader';
import COLORS from "../Constant/Colors";

export default class Loader extends Component{
    constructor(props){
        super(props);
    }
    
    render(){
        return <SkeletonLoader {...this.props} color={COLORS.LIGHT_GREY} highlightColor={COLORS.GREY}>
            {this.props.children}
        </SkeletonLoader >
    }
}