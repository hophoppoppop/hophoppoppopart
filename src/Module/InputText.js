import React,{Component} from 'react';
import COLORS from '../Constant/Colors';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import PropTypes  from 'prop-types';
import {Text, TextInput, View,Animated} from "react-native";
import {DatePicker, Icon} from "./index";
import moment from "moment";

const maxTop = 36;
const maxLeft = 10;

export default class InputText extends Component{

    static defaultProps = {
        title: "",
        value: "",
        error: "",
        action: ()=>{},
        multiline: false,
        maxLength: 45,
        isDate: false,
        secureTextEntry: false,
        keyboardType: "default",
        iconName:"",
        iconType: "fontAwesome",
        placeholder: "",
    };

    static propTypes = {
        title: PropTypes.any,
        value: PropTypes.any,
        error: PropTypes.any,
        action: PropTypes.any,
        multiline: PropTypes.bool,
        maxLength: PropTypes.any,
        isDate: PropTypes.bool,
        secureTextEntry: PropTypes.bool,
        keyboardType: PropTypes.any,
        iconName: PropTypes.any,
        iconType: PropTypes.any,
        placeholder: PropTypes.any
    };

    constructor(props){
        super(props);
        this.state = {
            isFocus: false,
            topTitle: new Animated.Value(maxTop),
            leftTitle: new Animated.Value(maxLeft)
        }
    }

    componentDidMount(){
    }
    
    render(){
        let {title,isDate,value,error,action,multiline,maxLength,secureTextEntry,keyboardType,iconName,placeholder,iconType} = this.props;
        return (
            <View style={{width:"100%",paddingTop:placeholder!==""?21:0}}>
                {title!==""?<Text style={{marginBottom:5,fontSize:16,fontWeight:"bold",color:this.state.isFocus?COLORS.BLUE:COLORS.BLACK}}>
                    {title}
                </Text>:null}
                {placeholder!==""?<Animated.Text
                    style={{position:"absolute",left:this.state.leftTitle,top:this.state.topTitle,color:this.state.isFocus || value !== ""?COLORS.BLUE:COLORS.GREY,fontWeight:this.state.isFocus || value !== ""?"bold":"200"}}
                >
                    {placeholder}
                </Animated.Text>:null}
                {
                    !isDate?
                        <View style={{width:"100%",flexDirection:"row",borderWidth:1,borderColor:this.state.isFocus?COLORS.BLUE:COLORS.GREY,borderRadius:10,padding:10,alignItems:"center"}}>
                            <View style={{flex:1}}>
                                <TextInput

                                    keyboardType={keyboardType}
                                    multiline={multiline}
                                    maxLength={maxLength}
                                    value={value} style={{padding: 0, margin: 0}}
                                    onChangeText={(text) => {
                                        action(text);
                                    }}
                                    secureTextEntry={secureTextEntry}
                                    allowFontScaling={false}
                                    /*placeholder={placeholder}*/
                                    onFocus={() => {
                                        this.setState({
                                            isFocus: true
                                        },()=>{
                                            Animated.parallel([
                                                Animated.timing(
                                                    this.state.topTitle,
                                                    {
                                                        toValue: 0,
                                                        duration: 100,
                                                    }
                                                ),
                                                Animated.timing(
                                                    this.state.leftTitle,
                                                    {
                                                        toValue: 0,
                                                        duration: 100,
                                                    }
                                                )
                                            ]).start();
                                        })
                                    }}
                                    onBlur={() => {
                                        this.setState({
                                            isFocus: false
                                        },()=>{
                                            if(this.props.value === "")
                                            {
                                                Animated.parallel([
                                                    Animated.timing(
                                                        this.state.topTitle,
                                                        {
                                                            toValue: maxTop,
                                                            duration: 100,
                                                        }
                                                    ),
                                                    Animated.timing(
                                                        this.state.leftTitle,
                                                        {
                                                            toValue: maxLeft,
                                                            duration: 100,
                                                        }
                                                    )
                                                ]).start();
                                            }
                                        })
                                    }}
                                />
                            </View>
                            {
                                iconName===""?null
                                :<Icon name={iconName} type={iconType} size={20} color={this.state.isFocus?COLORS.BLUE:COLORS.GREY} />
                            }
                        </View>
                        :
                        <DatePicker
                            style={{
                                borderWidth:1,
                                borderColor:COLORS.GREY,
                                borderRadius:10
                            }}
                            format={"DD-MMM-YYYY"}
                            date={value}
                            onDateChange={(date)=>{
                                action(date);
                            }}
                        />
                }
                {error !==""?<View style={{justifyContent:"center",alignSelf:"flex-end"}}>
                    <Text style={{color:COLORS.RED}}>
                        {error}
                    </Text>
                </View>:<View style={{height:5}}/>}
            </View>
        )
    }
}