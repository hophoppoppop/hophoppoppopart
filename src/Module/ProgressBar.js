import React,{Component} from 'react';
import {View,Animated} from 'react-native';
import COLORS from '../Constant/Colors';
import PropTypes  from 'prop-types';

export default class ProgressBar extends Component{

    /*
        progress has to be a decimal and below 1 (0.1,0.2,0.3,...)
    */

    static defaultProps = {
        progress: 0,
        height:20,
        width: 240,
        filledColor: COLORS.BLACK,
        filledStyle: {},
        emptyColor: COLORS.LIGHT_GREY,
        emptyStyle: {}
    };

    static propTypes = {
        progress: PropTypes.any,
        height: PropTypes.any,
        width: PropTypes.any,
        filledColor: PropTypes.any,
        filledStyle: PropTypes.any,
        emptyColor: PropTypes.any,
        emptyStyle: PropTypes.any
    };

    constructor(props){
        super(props);
        this.state={
            progress: new Animated.Value(0),
            layoutWidth: 0,
            layoutHeight: 0,
        }
    }

    componentDidUpdate(prevProps, prevState){
        let realProgress = this.props.progress*this.state.layoutWidth;
        let realPrevProgress = prevProps.progress*this.state.layoutWidth;
        if(realProgress !== realPrevProgress && this.state.layoutWidth !== 0)
        {
            Animated.spring(this.state.progress,{
               toValue: realProgress,
                duration: 2000,
            }).start();
        }
    }

    render(){
        return (
            <View
                style={[{width:this.props.width,height:this.props.height,backgroundColor:this.props.emptyColor},this.props.emptyStyle]}
                onLayout={e=>{
                    let widthLayout = e.nativeEvent.layout.width;
                    if(widthLayout !== 0 && this.state.layoutWidth === 0)
                    {
                        this.setState({
                            layoutWidth: widthLayout
                        },()=>{
                            Animated.spring(this.state.progress,{
                                toValue: this.props.progress*this.state.layoutWidth,
                                duration: 2000,
                            }).start();
                        })
                    }
                }}
            >
                <Animated.View style={[{width:this.state.progress,height:"100%",backgroundColor:this.props.filledColor},this.props.filledStyle]}/>
            </View>
        )
    }
}