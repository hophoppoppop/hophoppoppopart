
import React, {Component} from 'react';
import {
    View,
    Modal,
    TouchableOpacity,
    Text,
    TouchableWithoutFeedback
} from 'react-native';
import {observer} from 'mobx-react';
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import {Image,Icon} from "./index";

@observer
export default class ConfirmModal extends Component{

    constructor(props){
        super(props);
    }

    static toggleConfirmation(description,acceptAction,declineAction){
        modalStore.confirmModal = {
            visible : !modalStore.confirmModal.visible,
            description: description,
            acceptText: "Confirm",
            declineText: "Cancel",
            acceptAction: ()=>{
                modalStore.confirmModal.visible = false;
                if(acceptAction)
                    acceptAction();
            },
            declineAction: ()=>{
                modalStore.confirmModal.visible = false;
                if(declineAction)
                    declineAction();
            }
        };
    }

    closeConfirmModal(){
        modalStore.confirmModal.visible = false;
    }

    render(){
        return (
            <Modal
                transparent
                onRequestClose={() => {
                    this.closeConfirmModal();
                }}
                visible={modalStore.confirmModal.visible}>
                <TouchableOpacity onPress={()=>{
                    this.closeConfirmModal();
                }} style={{flex:1,backgroundColor: 'rgba(52, 52, 52, 0.8)',alignItems:"center",justifyContent:"flex-end"}}>
                    <TouchableWithoutFeedback>
                        <View style={{width:"100%",height:120,backgroundColor:COLORS.WHITE,borderTopLeftRadius:20,borderTopRightRadius:20}}>
                            <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                                <Text>{modalStore.confirmModal.description}</Text>
                            </View>
                            <View style={{width:"100%",height:40,flexDirection:"row"}}>
                                <TouchableOpacity style={{width:"50%",height:"100%", backgroundColor:COLORS.BLUE,alignItems:"center",justifyContent:"center"}} onPress={()=>{modalStore.confirmModal.acceptAction()}}>
                                    <Text style={{color:COLORS.WHITE}}>{modalStore.confirmModal.acceptText}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{width:"50%",height:"100%", backgroundColor:COLORS.LIGHT_GREY,alignItems:"center",justifyContent:"center"}} onPress={()=>{modalStore.confirmModal.declineAction()}}>
                                    <Text style={{color:COLORS.BLACK}}>{modalStore.confirmModal.declineText}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </TouchableOpacity>
            </Modal>
        )
    }
}