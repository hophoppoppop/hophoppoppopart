import React,{Component} from 'react';
import {
    TouchableOpacity,
    Text
} from 'react-native';
import DateTimePicker from 'react-native-datepicker'
import PropTypes  from 'prop-types';
import {DatePickerModal} from "./index";
import moment from "moment";

export default class DatePicker extends Component{

    static defaultProps = {
        dayLess: false,
        yearOnly: false,
        format: "DD MMMM YYYY",
        onDateChange:()=>{

        },
        style: null,
        date: moment(new Date())
    };

    static propTypes = {
        dayLess: PropTypes.bool,
        yearOnly: PropTypes.bool,
        format: PropTypes.any,
        onDateChange: PropTypes.any,
        style: PropTypes.any,
        date: PropTypes.any
    };

    constructor(props){
        super(props);
    }
    
    render(){
        return (
            <TouchableOpacity style={[{flex:1,padding:10,alignItems:"center",justifyContent:"center"},this.props.style]} onPress={()=>{
                DatePickerModal.toggleDate((date)=>{
                    this.props.onDateChange(date);
                },()=>{

                },this.props.date,{
                    yearOnly: this.props.yearOnly,
                    monthOnly: this.props.dayLess
                })
            }}>
                <Text style={{textAlign:"center"}}>{moment(this.props.date).format(this.props.format)}</Text>
            </TouchableOpacity>
        )
        /*if(this.props.dayLess || this.props.yearOnly)
        {
            return (
                <TouchableOpacity style={[{flex:1,padding:10,alignItems:"center",justifyContent:"center"},this.props.style]} onPress={()=>{
                    DatePickerModal.toggleDate((date)=>{
                        this.props.onDateChange(date);
                    },()=>{

                    },this.props.date,{
                        yearOnly: this.props.yearOnly,
                        monthOnly: this.props.dayLess
                    })
                }}>
                    <Text style={{textAlign:"center"}}>{moment(this.props.date).format(this.props.format)}</Text>
                </TouchableOpacity>
            )
        }else{
            return <DateTimePicker showIcon={false} {...this.props}/>
        }*/
    }
}