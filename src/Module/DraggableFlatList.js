
import React, {Component} from 'react';
import {
    View,
    StatusBar,
    FlatList,
    Animated,
    PanResponder,
    Text
} from 'react-native';
import {observer} from 'mobx-react';
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import Icon from 'react-native-vector-icons/FontAwesome';
import {Image} from "./index";
import PropTypes  from 'prop-types';

function immutableMove(arr, from, to) {
    return arr.reduce((prev, current, idx, self) => {
        if (from === to) {
            prev.push(current);
        }
        if (idx === from) {
            return prev;
        }
        if (from < to) {
            prev.push(current);
        }
        if (idx === to) {
            prev.push(self[from]);
        }
        if (from > to) {
            prev.push(current);
        }
        return prev;
    }, []);
}

@observer
export default class DraggableFlatList extends Component{

    static defaultProps = {
        dragAble: true,
        onDataChanged: (data)=>{

        },
        parentOffset: 0,
        scrollViewScrollOffset: 0,
        hasScrollView: false,
        data:[],
    };

    static propTypes = {
        dragAble: PropTypes.bool,
        onDataChanged: PropTypes.any,
        parentOffset: PropTypes.number,
        scrollViewScrollOffset: PropTypes.number,
        hasScrollView: PropTypes.bool,
        data: PropTypes.any
    };

    _panResponder: PanResponder.PanResponderInstance;
    point = new Animated.ValueXY();

    yToIndex(y){
        let propsScrollOffset = this.props.scrollViewScrollOffset < 0?0:this.props.scrollViewScrollOffset;
        let value = Math.floor((propsScrollOffset+this.state.scrollOffset + y - this.state.flatListTopOffset-this.props.parentOffset)/this.state.rowHeight);
        if(value < 0){
            return 0;
        }

        if(value > this.props.data.length-1){
            return this.props.data.length-1
        }

        return value;
    }

    constructor(props){
        super(props);
        this.state = {
            dragging: false,
            scrollOffset: 0,
            flatListTopOffset: 0,
            flatListHeight: 0,
            rowHeight:0,
            draggingIndex: -1,
            currentIndex: -1,
            currentY: 0,
            active: false,
        };
        this._panResponder = PanResponder.create({
            // Ask to be the responder:
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderGrant: (evt, gestureState) => {
                // The gesture has started. Show visual feedback so the user knows
                // what is happening!
                // gestureState.d{x,y} will be set to zero now
                Animated.event([{y:this.point.y}])({y:gestureState.y0-this.state.rowHeight/2-(this.props.parentOffset-this.props.scrollViewScrollOffset)});
                let index = this.yToIndex(gestureState.y0);
                this.setState({
                    currentY: gestureState.y0,
                    dragging: true,
                    active: true,
                    draggingIndex: index,
                    currentIndex: index,
                },()=>{
                    console.log("start");
                    this.animateList();
                })
            },
            onPanResponderMove: (evt, gestureState) => {
                let nextCoorY = gestureState.moveY-this.state.rowHeight/2-(this.props.parentOffset-this.props.scrollViewScrollOffset);
                let realHeight = this.state.flatListHeight-this.state.rowHeight;
                if (nextCoorY <= 0)
                {
                    Animated.event([{y:this.point.y}])({y:0});
                    this.setState({
                        currentY: gestureState.moveY
                    })
                }else if(nextCoorY >= realHeight)
                {
                    Animated.event([{y:this.point.y}])({y:realHeight});
                    this.setState({
                        currentY: gestureState.moveY
                    })
                }else{
                    Animated.event([{y:this.point.y}])({y:nextCoorY});
                    this.setState({
                        currentY: gestureState.moveY
                    })
                }
                // The most recent move distance is gestureState.move{X,Y}
                // The accumulated gesture distance since becoming responder is
                // gestureState.d{x,y}
            },
            onPanResponderTerminationRequest: (evt, gestureState) => true,
            onPanResponderRelease: (evt, gestureState) => {
                this.reset();
                // The user has released all touches while this view is the
                // responder. This typically means a gesture has succeeded
            },
            onPanResponderTerminate: (evt, gestureState) => {
                this.reset();
                // Another component has become the responder, so this gesture
                // should be cancelled
            },
            onShouldBlockNativeResponder: (evt, gestureState) => {
                // Returns whether this component should block native components from becoming the JS
                // responder. Returns true by default. Is currently only supported on android.
                return true;
            },
        });
    }

    animateList(){
        if(this.state.active)
        {
            requestAnimationFrame(()=>{
                const newIndex = this.yToIndex(this.state.currentY);
                if(this.state.currentIndex !== newIndex)
                {
                    console.log("reordering");
                    let data = immutableMove(this.props.data, this.state.currentIndex, newIndex);
                    this.props.onDataChanged(data);
                    this.setState({
                        currentIndex: newIndex,
                        draggingIndex: newIndex
                    })
                }
                this.animateList();
            })
        }else{
            return;
        }
    }

    reset(){
        this.setState({
            dragging: false,
            draggingIndex: -1,
            active: false
        })
    }

    render(){

        const draggableComponent = ()=>{
            return(
                <View {...!this.props.dragAble?{}:this._panResponder.panHandlers} style={{width:36,height:24,paddingLeft:12,justifyContent:"center"}}>
                    <Icon size={24} name={"ellipsis-v"}/>
                </View>
            )
        };

        const renderChildItem = (item)=>{
            if(this.props.renderItem(draggableComponent,item) !== null && this.props.renderItem(draggableComponent,item) !== undefined)
            {
                return (
                    <View
                        key={item.index}
                        onLayout={e=>{
                            this.setState({
                                rowHeight: e.nativeEvent.layout.height
                            })
                        }}
                        style={[{flexDirection:"row",opacity: this.state.draggingIndex === item.index?0:1},this.props.childrenStyle]}
                    >
                        {this.props.renderItem(draggableComponent,item)}
                    </View>
                )
            }else{
                return null;
            }
        };

        return (
            <View
                onLayout={e=>{
                    this.setState({
                        flatListTopOffset: e.nativeEvent.layout.y,
                        flatListHeight: e.nativeEvent.layout.height
                    })
                }}
            >
                {this.state.dragging?<Animated.View style={{position:"absolute",backgroundColor:COLORS.WHITE,width:"100%",zIndex: 2,top: this.point.getLayout().top}}>
                    {renderChildItem({item:this.props.data[this.state.draggingIndex]},true)}
                </Animated.View>:null}
                {!this.props.hasScrollView?
                    <FlatList
                    scrollEnabled={!this.state.dragging}
                    onScroll={ e => {
                        this.setState({
                            scrollOffset: e.nativeEvent.contentOffset.y
                        })
                    }}
                    scrollEventThrottle={16}
                    {...this.props}
                    renderItem={(item,index)=>renderChildItem(item,index)}/>
                    :
                    this.props.data.map((item,index)=>{
                        return renderChildItem({item,index})
                    })
                }
            </View>
        )
    }
}