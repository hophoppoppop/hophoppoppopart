
import React, {Component} from 'react';
import {
    View,
    Modal,
    TouchableOpacity,
    Text,
    ActivityIndicator
} from 'react-native';
import {observer} from 'mobx-react';
import modalStore from "../Stores/modalStore";
import LottieView from 'lottie-react-native';
import {CHECK_ANIM, LOADING_ANIM} from "../Assets";

@observer
export default class LoadingModal extends Component{

    constructor(props){
        super(props);
    }

    static toggleLoading(isLoading){
        modalStore.loadingModal = {
            visible : isLoading,
        };
    }

    render(){
        return (
            <Modal
                transparent
                visible={modalStore.loadingModal.visible}>
                <TouchableOpacity onPress={()=>{
                }} activeOpacity={1} style={{flex:1,backgroundColor: 'rgba(52, 52, 52, 0.8)',alignItems:"center",justifyContent:"center"}}>
                    <LottieView
                        style={{width:250,height:250}}
                        autoPlay
                        source={LOADING_ANIM}
                    />
                </TouchableOpacity>
            </Modal>
        )
    }
}