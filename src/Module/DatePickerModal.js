
import React, {Component} from 'react';
import {
    View,
    Modal,
    TouchableOpacity,
    Text,
    TouchableWithoutFeedback
} from 'react-native';
import {observer} from 'mobx-react';
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import {Image, Icon, ScrollPicker} from "./index";
import moment from "moment";

let getYear = parseInt(new Date().getFullYear());
let getMonth = new Date().getMonth();
let getDay = new Date().getDate();
let arrayYear = [];
let arrayDay = [];
for(let i = getYear-75;i<getYear+10;i++)
{
    arrayYear.push(i);
}

for(let i = 0;i<31;i++)
{
    arrayDay.push(i+1);
}

let arrayMonth = ["January","February","March","April","May","June","July","August","September","October","November","December"];

let indexDefaultDay = getDay-1;
let indexDefaultMonth = getMonth;
let indexDefaultYear = arrayYear.findIndex(s=>s===getYear);

function getDateCount(month, year){
    return new Date(year, month, 0).getDate();
}

@observer
export default class DatePickerModal extends Component{

    constructor(props){
        super(props);
    }

    static toggleDate(acceptAction,declineAction,defaultDate,props){
        let defaultDay = defaultDate?moment(defaultDate).format("DD"):null;
        let defaultMonth = defaultDate?moment(defaultDate).format("MMMM"):null;
        let defaultYear = defaultDate?moment(defaultDate).format("YYYY"):null;
        modalStore.datePickerModal = {
            selectedYearIndex: defaultYear?arrayYear.findIndex(s=>s.toString().toLowerCase() === defaultYear.toLowerCase()):indexDefaultYear,
            selectedMonthIndex: defaultMonth?arrayMonth.findIndex(s=>s.toLowerCase() === defaultMonth.toLowerCase()):indexDefaultMonth,
            selectedDayIndex: defaultDay?arrayDay.findIndex(s=>s.toString() === defaultDay.toString()):indexDefaultDay,
            acceptAction: (item)=>{
                modalStore.datePickerModal.visible = false;
                if(acceptAction)
                    acceptAction(item);
            },
            declineAction: ()=>{
                modalStore.datePickerModal.visible = false;
                if(declineAction)
                    declineAction();
            },
            visible : !modalStore.datePickerModal.visible,
            yearOnly: props === undefined || props.yearOnly === undefined?false:props.yearOnly,
            monthOnly: props === undefined || props.monthOnly === undefined?false:props.monthOnly
        };

    }

    closeDatePickerModal(){
        modalStore.datePickerModal.visible = false;
    }

    renderDatePicker(){
        if(modalStore.datePickerModal.yearOnly)
        {
            return (
                <View style={{width:"100%",flexDirection:"row",backgroundColor:COLORS.WHITE,borderTopLeftRadius:20,borderTopRightRadius:20}}>
                    <View onStartShouldSetResponder={() => true} style={{width:"100%",height:200}}>
                        <ScrollPicker
                            selectedIndex={modalStore.datePickerModal.selectedYearIndex}
                            data={arrayYear}
                            onValueChanged={(data,selectedIndex)=>{
                                modalStore.datePickerModal.selectedYearIndex = selectedIndex;
                            }}
                        />
                    </View>
                </View>
            )
        }else if(modalStore.datePickerModal.monthOnly){
            return (
                <View style={{width:"100%",flexDirection:"row",backgroundColor:COLORS.WHITE,borderTopLeftRadius:20,borderTopRightRadius:20}}>
                    <View onStartShouldSetResponder={() => true} style={{width:"50%",height:200}}>
                        <ScrollPicker
                            selectedIndex={modalStore.datePickerModal.selectedMonthIndex}
                            data={arrayMonth}
                            onValueChanged={(data,selectedIndex)=>{
                                modalStore.datePickerModal.selectedMonthIndex = selectedIndex;
                            }}
                        />
                    </View>
                    <View onStartShouldSetResponder={() => true} style={{width:"50%",height:200}}>
                        <ScrollPicker
                            selectedIndex={modalStore.datePickerModal.selectedYearIndex}
                            data={arrayYear}
                            onValueChanged={(data,selectedIndex)=>{
                                modalStore.datePickerModal.selectedYearIndex = selectedIndex;
                            }}
                        />
                    </View>
                </View>
            )
        }else{
            let dateCount = getDateCount(modalStore.datePickerModal.selectedMonthIndex+1,arrayYear[modalStore.datePickerModal.selectedYearIndex])-1;
            return (
                <View style={{width:"100%",flexDirection:"row",backgroundColor:COLORS.WHITE,borderTopLeftRadius:20,borderTopRightRadius:20,overflow:"hidden"}}>
                    <View onStartShouldSetResponder={() => true} style={{width:"33%",height:200}}>
                        <ScrollPicker
                            ref={ref=>this.dayPicker = ref}
                            selectedIndex={modalStore.datePickerModal.selectedDayIndex}
                            data={arrayDay.slice(0,dateCount+1)}
                            onValueChanged={(data,selectedIndex)=>{
                                modalStore.datePickerModal.selectedDayIndex = selectedIndex;
                            }}
                        />
                    </View>
                    <View onStartShouldSetResponder={() => true} style={{width:"33%",height:200}}>
                        <ScrollPicker
                            selectedIndex={modalStore.datePickerModal.selectedMonthIndex}
                            data={arrayMonth}
                            onValueChanged={(data,selectedIndex)=>{
                                modalStore.datePickerModal.selectedMonthIndex = selectedIndex;
                                let currDateCount = getDateCount(modalStore.datePickerModal.selectedMonthIndex+1,arrayYear[modalStore.datePickerModal.selectedYearIndex])-1;
                                if(currDateCount < modalStore.datePickerModal.selectedDayIndex+1)
                                {
                                    this.dayPicker.scrollToIndex(currDateCount);
                                }
                            }}
                        />
                    </View>
                    <View onStartShouldSetResponder={() => true} style={{width:"33%",height:200}}>
                        <ScrollPicker
                            selectedIndex={modalStore.datePickerModal.selectedYearIndex}
                            data={arrayYear}
                            onValueChanged={(data,selectedIndex)=>{
                                modalStore.datePickerModal.selectedYearIndex = selectedIndex;
                                let currDateCount = getDateCount(modalStore.datePickerModal.selectedMonthIndex+1,arrayYear[modalStore.datePickerModal.selectedYearIndex])-1;
                                if(currDateCount < modalStore.datePickerModal.selectedDayIndex+1)
                                {
                                    this.dayPicker.scrollToIndex(currDateCount);
                                }
                            }}
                        />
                    </View>
                </View>
            )
        }
    }

    render(){
        return (
            <Modal
                transparent
                onRequestClose={() => {
                    this.closeDatePickerModal();
                }}
                visible={modalStore.datePickerModal.visible}>
                <TouchableOpacity onPress={()=>{
                    this.closeDatePickerModal();
                }} style={{flex:1,backgroundColor: 'rgba(52, 52, 52, 0.8)',alignItems:"center",justifyContent:"flex-end"}}>
                    <TouchableWithoutFeedback>
                        <View style={{width:"100%",backgroundColor:COLORS.WHITE,borderTopLeftRadius:20,borderTopRightRadius:20}}>
                            {this.renderDatePicker()}
                            <View style={{width:"100%",height:40,flexDirection:"row"}}>
                                <TouchableOpacity style={{width:"50%",height:"100%", backgroundColor:COLORS.BLUE,alignItems:"center",justifyContent:"center"}} onPress={()=>{
                                    modalStore.datePickerModal.acceptAction(moment(arrayDay[modalStore.datePickerModal.selectedDayIndex]+" "+arrayMonth[modalStore.datePickerModal.selectedMonthIndex]+" "+arrayYear[modalStore.datePickerModal.selectedYearIndex],"DD MMMM YYYY").valueOf());

                                    this.closeDatePickerModal();
                                }}>
                                    <Text style={{color:COLORS.WHITE}}>Confirm</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{width:"50%",height:"100%", backgroundColor:COLORS.LIGHT_GREY,alignItems:"center",justifyContent:"center"}} onPress={()=> {
                                    modalStore.datePickerModal.declineAction()

                                    this.closeDatePickerModal();
                                }}>
                                    <Text style={{color:COLORS.BLACK}}>Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </TouchableOpacity>
            </Modal>
        )
    }
}