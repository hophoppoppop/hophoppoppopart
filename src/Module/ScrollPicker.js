
import React, {Component} from 'react';
import {
    View,
    Modal,
    TouchableOpacity,
    Text,
    TouchableWithoutFeedback
} from 'react-native';
import {observer} from 'mobx-react';
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import {Image,Icon} from "./index";
import WheelPicker from 'react-native-wheel-scroll-picker';
import PropTypes from'prop-types';

@observer
export default class ScrollPicker extends Component{


    static defaultProps = {
        data: [],
        onValueChanged: null,
        selectedIndex: 0
    };

    static propTypes = {
        data: PropTypes.any,
        onValueChanged: PropTypes.any,
        selectedIndex: PropTypes.any
    };

    constructor(props){
        super(props);
    };

    scrollToIndex(index){
        this.wheelPicker.scrollToIndex(index);
    }

    render(){
        return (
            <WheelPicker
                ref={(ref)=>this.wheelPicker = ref}
                dataSource={this.props.data}
                selectedIndex={this.props.selectedIndex}
                wrapperHeight={200}
                wrapperWidth={150}
                wrapperBackground={'#FFFFFF'}
                itemHeight={60}
                highlightColor={'#d8d8d8'}
                highlightBorderWidth={2}
                activeItemColor={'#222121'}
                itemColor={'#B4B4B4'}
                onValueChange={(data, selectedIndex) => {
                    if(this.props.onValueChanged) {
                        this.props.onValueChanged(data, selectedIndex);
                    }
                }}
            />
        )
    }
}