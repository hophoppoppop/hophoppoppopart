const COLORS = {
    RED: "#ff5963",
    GREEN: "#35C3B3",
    DARK_GREEN: "#569f56",
    WHITE: "#ffffff",
    BLUE: "#17BCD8",
    DARKER_BLUE: "#154051",
    DARK_BLUE: "#206078",
    YELLOW: "#feff8c",
    DARK_YELLOW: "#a8a961",
    ORANGE: "#ffef72",
    LIGHT_GREY: "#f0f0f0",
    GREY: "#d2d2d2",
    DARK_GREY:"#8e8e8e",
    BLACK:"#2e2e2e",
    TRANSPARENT: "transparent"
};

export default COLORS;