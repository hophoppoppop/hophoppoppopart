export const CARD_BACKGROUND = require('./Card_Background.jpg');

//LOTTIE ANIMATION
export const CHECK_ANIM = require('./LottieAnimation/Checklist');
export const LOADING_ANIM = require('./LottieAnimation/Loading');