
export async function getAPI(urlAction) {
    const fetchAPI = await fetch(urlAction, {
        method: 'GET',
    });
    return await fetchAPI.json();
}