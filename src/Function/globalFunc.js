import {GetCurrentScene, PagePop} from "../Routers/routerManager";
import backButtonStore from "../Stores/backButtonStore";
import {ToastAndroid} from 'react-native';

export function onBackButtonPressAndroid(){
    if (backButtonStore.backButton.disabledBackRoutes.find(s=>s === GetCurrentScene()) !== undefined) {
        return true;
    } else if(backButtonStore.backButton.closeBackRoutes.find(s=>s === GetCurrentScene()) !== undefined){
        if(backButtonStore.backButton.exitToastShow)
        {
            return false;
        }else{
            backButtonStore.backButton.exitToastShow = true;
            ToastAndroid.show('Press Back button once more to exit', ToastAndroid.SHORT);
            setTimeout(()=>{
                backButtonStore.backButton.exitToastShow = false;
            },2000);
            return true;
        }
    }else {
        if(backButtonStore.backButton.routerStack.length>0 )
        {
            if(backButtonStore.backButton.routerStack[backButtonStore.backButton.routerStack.length-1].action)
            {
                backButtonStore.backButton.routerStack[backButtonStore.backButton.routerStack.length-1].action();
            }else{
                PagePop();
            }
            backButtonStore.backButton.routerStack.pop();
        }
        return true;
    }
};