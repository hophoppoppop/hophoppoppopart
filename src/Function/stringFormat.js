
export function Capitalized(stringParam) {
    return stringParam.substring(0,1).toUpperCase() + stringParam.slice(1);
}

export function UnderLineToSpace(stringParam){
    let split = stringParam.split("_");
    split = split.map(item=>
        Capitalized(item)
    );
    let merge = "";
    split.map((item,index)=>{
        merge+=((index!==0?" ":"")+item);
    });
    return merge;
}

export function SpaceToUnderLine(stringParam){
    return stringParam.replace(/ /g,"_");
}