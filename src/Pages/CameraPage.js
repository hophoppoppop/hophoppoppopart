
import React, {Component} from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';
import {observer} from 'mobx-react';
import PropTypes  from 'prop-types';

import { RNCamera } from 'react-native-camera';
import {Header, Image, RootView,Icon} from "../Module";
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import {PagePop} from "../Routers/routerManager";

@observer
export default class CameraPage extends Component{

    constructor(props){
        super(props);
        this.state = {
            type: "back",
            photoURL: null,
        }
    }

    async takePicture(){
        if (this.camera) {
            let options = {
                fixOrientation: true,
                skipProcessing: true,
            };
            const data = await this.camera.takePictureAsync(options);
            this.setState({
                photoURL: data.uri
            })
        }
    }

    toggleFacing() {
        this.setState({
            type: this.state.type === 'back' ? 'front' : 'back',
        });
    }

    renderCamera(){
        if(this.state.photoURL === null)
        {
            return (

                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={{
                        flex:1
                    }}
                    type={this.state.type}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                >
                    <View style={{flex:1,marginBottom:10,justifyContent:"flex-end",alignItems:"center"}}>
                        <View style={{width:"100%",padding:40,height:70,justifyContent:"space-between",alignItems:"center",flexDirection:"row"}}>
                            <TouchableOpacity style={{width:70,height:70,borderRadius:35,justifyContent:"center",alignItems:"center"}} onPress={()=>{
                                PagePop();
                            }}>
                                <Icon name={"times"} size={30} color={COLORS.WHITE}/>
                            </TouchableOpacity>
                            <TouchableOpacity style={{width:70,height:70,borderRadius:35,justifyContent:"center",alignItems:"center",backgroundColor:COLORS.WHITE,opacity:0.7}} onPress={()=>{
                                this.takePicture();
                            }}>
                                <Icon name={"camera"} size={30} color={COLORS.BLACK}/>
                            </TouchableOpacity>
                            <TouchableOpacity style={{width:70,height:70,borderRadius:35,justifyContent:"center",alignItems:"center"}} onPress={()=>{
                                this.toggleFacing();
                            }}>
                                <Icon type={"material"} name={this.state.type !== 'back'?"camera-rear":"camera-front"} size={30} color={COLORS.WHITE}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </RNCamera>
            )
        }else{
            return (

                <View
                    style={{
                        flex:1
                    }}
                >
                    <View style={{flex:1}}>
                        <View style={{width:"100%",height:70}}/>
                        <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
                            <Image source={{uri:this.state.photoURL}} style={{width:"100%",height:"100%",resizeMode:"contain"}}/>
                        </View>
                        <View style={{width:"100%",padding:40,height:70,justifyContent:"space-between",alignItems:"center",flexDirection:"row"}}>
                            <TouchableOpacity style={{width:70,height:70,borderRadius:35,justifyContent:"center",alignItems:"center"}} onPress={()=>{
                                this.setState({
                                    photoURL: null,
                                })
                            }}>
                                <Icon name={"times"} size={30} color={COLORS.RED}/>
                            </TouchableOpacity>
                            <View style={{width:70,height:70}}/>
                            <TouchableOpacity style={{width:70,height:70,borderRadius:35,justifyContent:"center",alignItems:"center"}} onPress={()=>{
                                if(this.props.action)
                                {
                                    this.props.action(this.state.photoURL);
                                }else{
                                    PagePop();
                                }
                            }}>
                                <Icon name={"check"} size={30} color={COLORS.GREEN}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            )
        }
    }

    render(){
        return (
            <RootView barColor={COLORS.BLACK} barStyle={"light-content"}>
                {this.renderCamera()}
            </RootView>
        )
    }
}