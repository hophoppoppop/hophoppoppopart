
import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    Text, AsyncStorage,
    Clipboard,
    TouchableWithoutFeedback,
    Keyboard
} from 'react-native';
import {observer} from 'mobx-react';
import PropTypes  from 'prop-types';

import Swiper from 'react-native-swiper';
import {Header, Icon, InputText, LoadingModal, RootView} from "../Module";
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import firebase from '@react-native-firebase/app';
import {auth} from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import otpStore from "../Stores/otpStore";
import * as RouterList from "../Routers/routerList";
import {RouterDirector} from "../Routers/routerManager";
import loginStore from "../Stores/loginStore";

let CryptoJS = require("crypto-js");
let key = "hophoppoppop";

@observer
export default class LoginPage extends Component{

    constructor(props){
        super(props);
        this.state={
            handphone: "",
            password: "",
            autoVerif: false,
        }
    }

    componentDidMount(){
        firebase.auth().onAuthStateChanged((user)=>{
            //set the user accrodingly
            if(user && user.phoneNumber !== null && user.phoneNumber !== undefined)
            {
                loginStore.loginStore = user;
                this.setState({
                    autoVerif: true,
                },()=>{
                    LoadingModal.toggleLoading(false);
                    RouterDirector(RouterList.SuccessPage,{
                        successSentence: "Sign in Success"
                    });
                });
            }
        });
    }

    sendOTP(phoneNumber,action){
        firebase.auth().signInWithPhoneNumber(phoneNumber,true).then(confirmResult=>{
            otpStore.confirmResult = confirmResult;
            action();
            /*confirmResult.confirm("12345")
                .then(user => {
                    console.log(user)
                }).catch(error => console.log(error));*/
        }).catch(err=>console.log("Error Login: "+err));
    }

    render(){
        return (
            <RootView barColor={COLORS.GREEN} barStyle={"light-content"}>
                <Header headerColor={COLORS.GREEN} backButtonColor={COLORS.WHITE} noBorder/>

                <TouchableWithoutFeedback
                    onPress={()=>{
                        Keyboard.dismiss();
                    }}
                >
                    <View style={{flex:1,backgroundColor:COLORS.GREEN,justifyContent:"center"}}>
                        <View style={{width:"100%",alignItems:"center",paddingHorizontal:20}}>
                            <View style={{width:"100%",marginBottom:20,paddingTop:50,alignItems:"center",overflow:"visible",borderRadius:10,borderWidth:1,borderColor:COLORS.GREY,backgroundColor:COLORS.WHITE}}>
                                <View style={{position:"absolute",top:-50,borderWidth:1,borderColor:COLORS.WHITE,width:100,height:100,borderRadius:50,alignItems:"center",justifyContent:"center",backgroundColor:COLORS.BLUE}}>
                                    <Icon size={80} color={COLORS.WHITE} name={"user"}/>
                                </View>
                                <View style={{width:"100%",paddingHorizontal:30,paddingBottom:20}}>
                                    <View style={{width:"100%"}}>
                                        <InputText
                                            keyboardType={"numeric"}
                                            value={this.state.handphone}
                                            title={""}
                                            action={(text)=>{
                                                this.setState({
                                                    handphone:text
                                                })
                                            }}
                                            placeholder={"Mobile Phone"}
                                            iconName={"cellphone"}
                                            iconType={"materialCommunity"}
                                        />
                                    </View>
                                    <View style={{width:"100%"}}>
                                        <InputText
                                            secureTextEntry
                                            value={this.state.password}
                                            title={""}
                                            action={(text)=>{
                                                this.setState({
                                                    password:text
                                                })
                                            }}
                                            placeholder={"Password"}
                                            iconName={"key"}
                                            iconType={"materialCommunity"}
                                        />
                                    </View>
                                </View>
                                <View style={{width:"100%",paddingHorizontal:30,paddingBottom:10}}>
                                    <TouchableOpacity
                                        style={{width:"100%",backgroundColor:COLORS.BLUE,borderRadius:15,alignItems:"center",justifyContent:"center",padding:15}}
                                        onPress={async ()=>{
                                            LoadingModal.toggleLoading(true);
                                            const adminData = await firestore()
                                                .collection('admin')
                                                .get();
                                            if(adminData)
                                            {
                                                const users = adminData.docs.map((docsData) => {
                                                    return {
                                                        ...docsData.data(),
                                                    };
                                                });
                                                if(this.state.handphone !== "" && this.state.password !== "" && users.find(s=>s.phoneNumber.toLowerCase() === this.state.handphone && CryptoJS.AES.decrypt(s.password,key).toString(CryptoJS.enc.Utf8) === this.state.password) !== undefined)
                                                {
                                                    let countryPhonenumber = "+62"+this.state.handphone.slice(1,this.state.handphone.length);
                                                    this.sendOTP(countryPhonenumber,()=>{
                                                        setTimeout(()=>{
                                                            if(!this.state.autoVerif)
                                                            {
                                                                LoadingModal.toggleLoading(false);
                                                                RouterDirector(RouterList.OtpPage,{
                                                                    sendOtp: ()=>{
                                                                        this.sendOTP(countryPhonenumber);
                                                                    }
                                                                });
                                                            }
                                                        },1000);
                                                    });
                                                }else{
                                                    LoadingModal.toggleLoading(false);
                                                    alert("Username or password wrong!");
                                                }
                                            }
                                        }}
                                    >
                                        <Text style={{color:COLORS.WHITE,fontSize:16}}>
                                            Login
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </RootView>
        )
    }
}