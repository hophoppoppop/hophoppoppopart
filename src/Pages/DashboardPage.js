import React, {Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    ImageBackground,
    Linking,
    AsyncStorage,
    Animated
} from 'react-native';
import COLORS from "../Constant/Colors";
import {getAPI} from "../Function/api";
import {CARD_BACKGROUND} from "../Assets";

import ProfileStore from "../Stores/profileStore";

import Collapsible from 'react-native-collapsible';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';
import firebase from '@react-native-firebase/app';
import {storage} from '@react-native-firebase/storage';
import {auth} from '@react-native-firebase/auth';
import {observer} from 'mobx-react';
import moment from 'moment';
import {ImageViewer, Image, RootView, Loader, Icon, ScrollPicker, LoadingModal,ProgressBar} from "../Module";
import {RouterDirector} from "../Routers/routerManager";
import * as RouterList from "../Routers/routerList";
import {Capitalized, UnderLineToSpace} from "../Function/stringFormat";
import loginStore from "../Stores/loginStore";

let {width, height} = Dimensions.get("window");

let arrayColor = [COLORS.GREEN,COLORS.BLUE,COLORS.YELLOW,COLORS.ORANGE]

const defaultApp = firebase.app();
const defaultStorage = defaultApp.storage();

const profileJSON = defaultStorage.ref("profile.json");
const profileFoto = defaultStorage.ref("profile.jpg");
const artFolder = defaultStorage.ref("Art");
const appsJSON = defaultStorage.ref("appsList.json");
const lastUpdatedJSON = defaultStorage.ref("lastUpdated.json");

function hashCode(str) { // java String#hashCode
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    let colour = '#';
    for (let i = 0; i < 3; i++) {
        let value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}

@observer
export default class DashboardPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            profileData: null,
            profilePhoto: null,
            artPhoto: null,
            appsData: {
                dataGameApps: null,
                dataMobileApps: null,
            },
            collapseProfile: false,
            collapseArt: false,
            collapseMobileApps: false,
            collapseGameApps: false,
            editMode: false,
            editModeOnAnim: false,
            confirmPosition: new Animated.Value(-120),
        }
    }

    async loginAuth(){
        let isLogin = await AsyncStorage.getItem("SignStatus") === "login";
        if(!isLogin) {
            firebase.auth().signInAnonymously().then(() => {
                AsyncStorage.setItem("SignStatus", "login");
            });
        }
    }

    async componentDidMount() {
        if(ProfileStore.profileData !== null && ProfileStore.profileData !== undefined)
        {
            Animated.spring(
                this.state.confirmPosition,
                {
                    toValue: 20,
                    duration: 1000,
                }
            ).start();
        }
        firebase.auth().onAuthStateChanged(async (user)=>{
            if(user){
                loginStore.userData = user;
                this.loadData();
            }else{
                await AsyncStorage.setItem("SignStatus", "logout");
                this.loginAuth();
            }
        });
    }

    loadData(){
        this.loadArt();
        this.loadApps();
        this.loadProfile();
        this.loadLastUpdated();
    }

    async loadProfile(){
        let photoURL = await profileFoto.getDownloadURL();
        getAPI(await profileJSON.getDownloadURL()).then(async (callback) => {
            ProfileStore.profileData = callback;
            ProfileStore.profilePhoto = photoURL;
            Animated.spring(
                this.state.confirmPosition,
                {
                    toValue: 20,
                    duration: 1000,
                }
            ).start();
        });
    }

    async loadApps(){
        getAPI(await appsJSON.getDownloadURL()).then(async (callback) => {
            this.setState({
                appsData: callback,
            })
        });
    }

    async loadLastUpdated(){
        getAPI(await lastUpdatedJSON.getDownloadURL()).then(async (callback) => {
            ProfileStore.lastUpdated = parseInt(callback.lastUpdated);
        });
    }

    async loadArt(){
        let artList = await artFolder.listAll();
        let tempArt = [];
        tempArt = await Promise.all(artList._items.map((item)=>{
            return Promise.resolve(defaultStorage.ref(item.path).getDownloadURL());
        }));
        this.setState({
            artPhoto: tempArt,
        })
    }

    checkLastUpdate(date){
        let millisecondDate = new Date(date).valueOf();
        if(parseInt(millisecondDate) > ProfileStore.lastUpdated)
        {
            return millisecondDate;
        }else{
            return ProfileStore.lastUpdated;
        }
    }

    renderProfilePhoto(){

        return (
            <TouchableOpacity style={styles.profilePhotoStyle} onPress={()=>{
                ImageViewer.toggleImage(ProfileStore.profilePhoto);
            }}>
                <Loader size={width * 0.2} loading={ProfileStore.profilePhoto === null} type={"circle"}>
                    <Image style={{width: "100%", height: "100%",borderRadius:width * 0.1}}
                           source={{uri: ProfileStore.profilePhoto}}/>
                </Loader>
            </TouchableOpacity>
        )
    }

    renderProfileName(){
        let isProfileDataLoading = (ProfileStore.profileData === null);
        return (
            <View style={{flex:isProfileDataLoading?0.7:0,height: "100%", marginHorizontal: 15, marginTop: isProfileDataLoading?3:-5, justifyContent: "center"}}>
                <Loader type="rectangle" rows={1} height={24} loading={isProfileDataLoading}>
                    <Text style={{
                        color: COLORS.BLACK,
                        borderBottomWidth: 1,
                        borderColor: COLORS.BLACK,
                        fontWeight: "bold",
                        fontSize: 24,
                    }}>{!isProfileDataLoading ? ProfileStore.profileData.profile.find(s=>s.key === "name").value : ""}</Text>
                </Loader>
                <Loader type="rectangle" rows={1} height={12} loading={isProfileDataLoading}>
                    <Text style={{
                        flex: 0,
                        color: COLORS.BLACK,
                        fontWeight: "bold",
                        fontSize: 12,
                    }}>{!isProfileDataLoading ? ProfileStore.profileData.work_experience[0].description : ""}</Text>
                </Loader>
            </View>
        )
    }

    renderDotList(data,component){
        return (
            <View style={{flex: 1,marginTop:5}}>
                {data.map((subItem,index)=>
                    <View key={index} style={{  flexDirection:"row"}}>
                        <View style={{width: 15, alignItems: "center",marginRight:10}}>
                            <View style={{width:15,height:15,borderRadius:8,backgroundColor:arrayColor[index]}}/>
                            {
                                (index !== data.length - 1) ?
                                    <LinearGradient colors={[arrayColor[index], arrayColor[index+1]]} style={{
                                        width: 2,
                                        flex:1,
                                        backgroundColor: COLORS.TRANSPARENT
                                    }}/>
                                    :
                                    null
                            }
                        </View>
                        {component
                            ?
                            <View style={{flex:1,marginLeft:10,marginTop:-3,marginBottom:10}}>
                                {component(subItem)}
                            </View>
                            :
                            null}
                    </View>
                )}
            </View>
        )
    }

    renderProfileItems(){

        return ProfileStore.profileData.profile.map((item,index)=> {
            return (
                <View style={styles.profileItem} key={index}>
                    <View>
                        <Text style={styles.profileItemTitleFont}>
                            {Capitalized(UnderLineToSpace(item.key))}
                        </Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.profileItemFont}>
                            {item.key.toLowerCase().indexOf("date") !== -1 ? moment(item.value).format("DD-MM-YYYY") :item.value}
                        </Text>
                    </View>
                </View>
            )
        })
    }

    renderWorkExperienceItems(){
        return (
            <View style={styles.profileItemFull}>
                <View>
                    <Text style={styles.profileItemTitleFont}>
                        Work Experience
                    </Text>
                </View>
                {this.renderDotList(ProfileStore.profileData.work_experience,(subItem)=>{
                    return (
                        <View>
                            <Text style={styles.workDescription}>
                                {subItem.description}
                            </Text>
                            <Text style={styles.workDate}>
                                {moment(subItem.start_date).format("MMM YYYY")} - {subItem.end_date === "-"?"Present":moment(subItem.end_date).format("MMM YYYY")}
                            </Text>
                            <Text style={styles.profileItemFont}>
                                {subItem.company}
                            </Text>
                            <Text style={styles.workAddress}>
                                {subItem.location}
                            </Text>
                        </View>
                    )
                })}
            </View>
        )
    }

    renderEducationItems(){
        return (

            <View style={styles.profileItemFull}>
                <View>
                    <Text style={styles.profileItemTitleFont}>
                        Education
                    </Text>
                </View>
                {this.renderDotList(ProfileStore.profileData.education,(subItem)=>{
                    return (
                        <View>
                            <Text style={styles.workDescription}>
                                {subItem.name}
                            </Text>
                            <Text style={styles.workDate}>
                                {moment(subItem.start_year).format("YYYY")} - {subItem.end_year === "-"?"Present":moment(subItem.end_year).format("YYYY")}
                            </Text>
                            <Text style={styles.profileItemFont}>
                                {subItem.degree}
                            </Text>
                            <Text style={styles.profileItemFont}>
                                {subItem.field}
                            </Text>
                            <Text style={styles.workAddress}>
                                {subItem.grade} GPA
                            </Text>
                        </View>
                    )
                })}
            </View>
        )
    }

    renderSkillItems(){
        if(ProfileStore.profileData.skill !== undefined && ProfileStore.profileData.skill !== null)
        {
            return (
                <View style={styles.profileItemFull}>
                    <View>
                        <Text style={styles.profileItemTitleFont}>
                            Skills
                        </Text>
                    </View>
                    {
                        ProfileStore.profileData.skill.map((item,index)=>{
                            return (
                                <View style={{marginBottom:5}} key={index.toString()}>
                                    <Text>{item.name}</Text>
                                    <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center",marginTop:5}}>
                                        <View style={{flex:1,}}>
                                            <ProgressBar
                                                width={"100%"}
                                                height={20}
                                                filledStyle={{borderRadius:10}}
                                                emptyStyle={{borderRadius:10}}
                                                progress={item.percent/100}
                                                filledColor={hashCode(item.name)}
                                            />
                                        </View>
                                        <View style={{marginHorizontal: 10}}>
                                            <Text>{item.percent}%</Text>
                                        </View>
                                    </View>
                                </View>
                            )
                        })
                    }
                </View>
            )
        }else{
            return null;
        }
    }

    renderFullProfile(){
        let isProfileLoading = ProfileStore.profileData === null;
        let dummy = [0,0,0,0,0];
        if(!isProfileLoading)
        {
            return (
                <View style={{flex:1,flexDirection: "row", flexWrap: "wrap"}}>
                    {this.renderProfileItems()}
                    {this.renderWorkExperienceItems()}
                    {this.renderEducationItems()}
                    {this.renderSkillItems()}
                </View>
            )
        }else{
            return (
                <View style={{flex:1,width:"100%",paddingTop:10,flexDirection:"row"}}>
                    <View style={{width:"50%"}}>
                        {dummy.map((item,index)=>
                            <View key={index} style={{width:"100%"}}>
                                <View style={{width:"50%"}}>
                                    <Loader rows={1} height={14} type={"rectangle"} loading={isProfileLoading}/>
                                </View>
                                <View style={{width:"60%"}}>
                                    <Loader rows={1} height={12} type={"rectangle"} loading={isProfileLoading}/>
                                </View>
                            </View>
                        )}
                    </View>
                    <View style={{width:"50%"}}>
                        {dummy.map((item,index)=>
                            <View key={index} style={{width:"100%"}}>
                                <View style={{width:"50%"}}>
                                    <Loader rows={1} height={14} type={"rectangle"} loading={isProfileLoading}/>
                                </View>
                                <View style={{width:"60%"}}>
                                    <Loader rows={1} height={12} type={"rectangle"} loading={isProfileLoading}/>
                                </View>
                            </View>
                        )}
                    </View>
                </View>
            )
        }
    }

    renderMobileApps(data,collapseBool){
        let isMobileAppsLoading = (data === null);
        let dummy = [0,0];
        if(!isMobileAppsLoading)
        {
            return (
                <Collapsible style={{width: "100%", flexDirection: "row", flexWrap: "wrap"}} collapsed={collapseBool}>
                    {data.map((item, index) =>
                        <TouchableOpacity key={index} style={styles.artItem} onPress={() => {
                            Linking.openURL("market://details?id="+item.link);
                        }}>
                            <View style={styles.appsItem}>
                                <Image style={{
                                    width:"75%",
                                    aspectRatio:1,
                                    resizeMode: "cover",
                                }} source={{uri: item.icon}}/>
                                <Text style={{fontWeight:"bold",marginHorizontal: 10,marginTop:5,textAlign:"center"}} numberOfLines={1} ellipsizeMode={"tail"}>{item.name}</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                </Collapsible>

            )
        }else{
            return <View style={{width: "100%", flexDirection: "row", flexWrap: "wrap"}}>
                {
                    dummy.map((item,index)=>
                        this.renderLoaderItem(isMobileAppsLoading,index)
                    )
                }
            </View>
        }
    }

    renderArt(){
        let isArtLoading = (this.state.artPhoto === null);
        let dummy = [0,0];
        if(!isArtLoading)
        {
            return this.state.artPhoto.map((item,index)=>
                        <TouchableOpacity key={index} style={styles.artItem} onPress={()=>{
                            RouterDirector(RouterList.ImageViewerPage,{multiple: true,photo:this.state.artPhoto,currIndex: index,title:"Art"});
                        }}>
                            <Image style={{width:"100%",height:"100%",resizeMode:"cover",borderRadius:5,borderWidth:1,borderColor:COLORS.GREY}} source={{uri:item}}/>
                        </TouchableOpacity>
                    )
        }else{
            return dummy.map((item,index)=>
                this.renderLoaderItem(isArtLoading,index)
            )
        }
    }

    renderLoaderItem(loadingBool,index){
        return (
            <View key={index} style={styles.artItem}>
                <Loader size={width*0.5-40} type={"square"} loading={loadingBool}/>
            </View>
        )
    }

    toggleEditMode(){
        if(loginStore.userData && loginStore.userData.phoneNumber !== null && !loginStore.userData.isAnonymous)
        {
            RouterDirector(RouterList.EditPage);
        }else{
            RouterDirector(RouterList.LoginPage);
        }
        /*this.setState({
            editMode: !this.state.editMode,
        },()=>{
            if(this.state.editMode)
            {
                Animated.spring(
                    this.state.confirmPosition,
                    {
                        toValue: 80,
                        duration: 1000,
                    }
                ).start();
            }else{
                Animated.spring(
                    this.state.confirmPosition,
                    {
                        toValue: 20,
                        duration: 1000,
                    }
                ).start();
            }
        });*/
    }

    render() {
        return (
            <RootView barColor={COLORS.WHITE} barStyle={"dark-content"}>
                <ScrollView>
                    <TouchableOpacity activeOpacity={1} onPress={() => {
                        this.setState({
                            collapseProfile: !this.state.collapseProfile
                        })
                        // RouterDirector(RouterList.ProfilePage);
                    }} style={{
                        flex: 1,
                        padding: 10
                    }}>
                        <View style={{
                            backgroundColor: COLORS.WHITE,
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 1,
                            },
                            shadowOpacity: 0.20,
                            shadowRadius: 1.41,

                            elevation: 2,
                            borderRadius: 5,
                            overflow: "hidden",
                        }}>
                            <ImageBackground
                                style={{
                                    flexDirection: "row",
                                    height: "100%",
                                }} source={CARD_BACKGROUND}>
                                {this.renderProfilePhoto()}
                                {this.renderProfileName()}
                            </ImageBackground>
                        </View>
                    </TouchableOpacity>
                    <View style={{marginTop: 5}}>
                        <View style={styles.menuContainer}>
                            <TouchableOpacity onPress={() => {
                                this.setState({
                                    collapseProfile: !this.state.collapseProfile
                                })
                            }} style={[{flexDirection:"row"},!this.state.collapseProfile?styles.menuContainerBorderTitle:null]}>
                                <Text style={styles.menuContainerTitleFont}>
                                    Profile
                                </Text>
                                <View style={{flex:1,alignItems:"flex-end",justifyContent:"center"}}>
                                    <Icon size={20} name={this.state.collapseProfile?"chevron-down":"chevron-up"}/>
                                </View>
                            </TouchableOpacity>
                            <Collapsible collapsed={this.state.collapseProfile}>
                                {this.renderFullProfile()}
                            </Collapsible>
                        </View>
                        <View style={styles.menuContainer}>
                            <TouchableOpacity onPress={() => {
                                this.setState({
                                    collapseArt: !this.state.collapseArt
                                })
                            }} style={[{flexDirection:"row"},!this.state.collapseArt?styles.menuContainerBorderTitle:null]}>
                                <Text style={styles.menuContainerTitleFont}>
                                    Art
                                </Text>
                                <View style={{flex:1,alignItems:"flex-end",justifyContent:"center"}}>
                                    <Icon size={20} name={this.state.collapseArt?"chevron-down":"chevron-up"}/>
                                </View>
                            </TouchableOpacity>
                            <Collapsible style={{width:"100%",flexDirection: "row", flexWrap: "wrap"}}
                                         collapsed={this.state.collapseArt}>
                                {this.renderArt()}
                            </Collapsible>
                        </View>
                        <View style={styles.menuContainer}>
                            <TouchableOpacity onPress={() => {
                                this.setState({
                                    collapseMobileApps: !this.state.collapseMobileApps
                                })
                            }} style={[{flexDirection: "row"}, !this.state.collapseMobileApps ? styles.menuContainerBorderTitle : null]}>
                                <Text style={styles.menuContainerTitleFont}>
                                    Mobile Apps
                                </Text>
                                <View style={{flex: 1, alignItems: "flex-end", justifyContent: "center"}}>
                                    <Icon size={20} name={this.state.collapseMobileApps ? "chevron-down" : "chevron-up"}/>
                                </View>
                            </TouchableOpacity>
                            {this.renderMobileApps(this.state.appsData.dataMobileApps,this.state.collapseMobileApps)}
                        </View>
                        <View style={styles.menuContainer}>
                            <TouchableOpacity onPress={() => {
                                this.setState({
                                    collapseGameApps: !this.state.collapseGameApps
                                })
                            }}
                                              style={[{flexDirection: "row"}, !this.state.collapseGameApps ? styles.menuContainerBorderTitle : null]}>
                                <Text style={styles.menuContainerTitleFont}>
                                    Game Apps
                                </Text>
                                <View style={{flex: 1, alignItems: "flex-end", justifyContent: "center"}}>
                                    <Icon size={20} name={this.state.collapseGameApps ? "chevron-down" : "chevron-up"}/>
                                </View>
                            </TouchableOpacity>
                            {this.renderMobileApps(this.state.appsData.dataGameApps,this.state.collapseGameApps)}
                        </View>
                    </View>
                    <Text style={{marginRight:10,marginBottom:10,textAlign:"right",fontSize:10,color:COLORS.GREY}}>last Updated {ProfileStore.lastUpdated === 0?"------":moment(ProfileStore.lastUpdated).format("DD MMM YYYY")}</Text>
                </ScrollView>
                {/*<Animated.View style={{position:"absolute",bottom:20,right:this.state.confirmPosition}}>
                    <TouchableOpacity style={{width:50,height:50,backgroundColor:COLORS.BLUE,borderRadius:25,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.23,
                        shadowRadius: 2.62,

                        elevation: 4,alignItems:"center",justifyContent:"center"}}
                                      onPress={()=>{
                                          this.toggleEditMode();
                                      }}>
                        <Icon name={"check"} size={24} color={COLORS.WHITE}/>
                    </TouchableOpacity>
                </Animated.View>*/}
                <Animated.View style={{position:"absolute",bottom:20,right:this.state.confirmPosition,width:50,height:50,backgroundColor:COLORS.BLUE,borderRadius:25,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,

                    elevation: 4,alignItems:"center",justifyContent:"center"}}>
                    <TouchableOpacity style={{width:"100%",height:"100%",alignItems:"center",justifyContent:"center"}}
                                      onPress={()=>{
                                          this.toggleEditMode();
                                      }}>
                        <Icon name={"pencil"} size={24} color={COLORS.WHITE}/>
                    </TouchableOpacity>
                </Animated.View>
            </RootView>
        )
    }
}

const styles = StyleSheet.create({
    menuContainer: {
        flex: 1,
        justifyContent: "center",
        padding: 10,
        marginHorizontal: 10,
        marginBottom: 10,
        borderRadius: 5,
        borderWidth: 1,
        borderColor:COLORS.GREY
    },
    profilePhotoStyle: {
        width: width * 0.2,
        height: width * 0.2,
        borderRadius: width * 0.1,
        backgroundColor: COLORS.WHITE,
        overflow:"hidden",
        alignItems: "center",
        justifyContent: "center",
        marginVertical: 10,
        marginLeft: 20
    },
    menuContainerTitleFont: {fontSize: 24, fontWeight: "bold"},
    menuContainerBorderTitle: {borderBottomWidth: 1,borderColor:COLORS.GREY, paddingBottom:10,marginBottom:5},
    profileItem: {width: "50%", marginBottom: 5,},
    artItem: {width:"50%",aspectRatio: 1,alignItems:"center",justifyContent:"center",padding:5},
    appsItem: {width:"100%",height:"100%",borderRadius: 5, borderWidth: 1, borderColor: COLORS.GREY,alignItems:"center",justifyContent:"center"},
    profileItemFull: {width: "100%"},
    profileItemTitleFont: {fontSize: 16,fontWeight:"bold"},
    profileItemFont: {fontSize: 12,},
    workDescription: {fontSize: 14, fontWeight:"bold"},
    workDate: {fontSize:12,fontWeight:"bold"},
    workAddress: {fontSize:12,fontWeight:"100"}
})
