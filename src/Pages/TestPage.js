
import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import {observer} from 'mobx-react';
import {DraggableFlatList} from "../Module";

@observer
export default class CameraPage extends Component{

    constructor(props){
        super(props);
        this.state = {
            data: Array.from(Array(10), (_,i)=>i)
        }
    }

    render(){
        return (
            <View style={{flex:1}}>
                <DraggableFlatList
                    data={this.state.data}
                    childrenStyle={{flex:1,padding:10,}}
                    keyExtractor={(item,index)=>index.toString()}
                    onDataChanged={(data)=>{
                        this.setState({
                            data: data
                        })
                    }}
                    renderItem={(draggable,{item})=>{
                        return (
                            <View style={{flex:1,alignItems:"center",borderWidth:1,padding:10,backgroundColor:"white",flexDirection:"row"}}>
                                {draggable()}
                                <Text>
                                    {item}
                                </Text>
                            </View>
                        )
                    }}
                />
            </View>
        )
    }
}