
import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    Text, AsyncStorage,
    Clipboard,
    TextInput
} from 'react-native';
import {observer} from 'mobx-react';
import PropTypes  from 'prop-types';

import Swiper from 'react-native-swiper';
import {Header, Icon, LoadingModal, RootView} from "../Module";
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import firebase from '@react-native-firebase/app';
import {auth} from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import otpStore from "../Stores/otpStore";
import {RouterDirector} from "../Routers/routerManager";
import * as RouterList from "../Routers/routerList";

let CryptoJS = require("crypto-js");
let key = "hophoppoppop";

let dummyPin = [1,2,3,4,5,6];
let dummyKeyboard = [1,2,3,4,5,6,7,8,9];

@observer
export default class OtpPage extends Component{

    constructor(props){
        super(props);
        this.state={
            otp: "",
            countResend: 60
        }
    }

    intervalID = 0;

    componentDidMount(){
        this.recount();
    }

    recount(){
        this.intervalID = setInterval(()=>{
            this.setState({
                countResend: this.state.countResend - 1,
            },()=>{
                if(this.state.countResend <= 0)
                {
                    clearInterval(this.intervalID);
                }
            })
        },1000);
    }

    insertOTP(number){
        if(this.state.otp.length < dummyPin.length)
        {
            this.setState({
                otp: this.state.otp+number
            },()=>{
                if(this.state.otp.length === dummyPin.length)
                {
                    if(otpStore.confirmResult)
                    {
                        LoadingModal.toggleLoading(true);
                        otpStore.confirmResult.confirm(this.state.otp).then(user=>{
                            if(user)
                            {
                                LoadingModal.toggleLoading(false);
                                RouterDirector(RouterList.SuccessPage,{
                                    successSentence: "Sign in Success"
                                });
                            }
                        }).catch(err=>{
                            console.log("Error OTP: "+err);
                        })
                    }
                }
            })
        }
    }

    render(){
        return (
            <RootView>
                <Header noBorder/>
                <View
                    style={{flex:1}}
                >
                    <View style={{flex:1,alignItems:"center",}}>
                        <View style={{marginBottom:50,paddingHorizontal:20}}>
                            <Text style={{fontSize:26,fontWeight: "bold",textAlign:"center"}}>
                                Verification Code
                            </Text>
                            <Text style={{fontSize:16,marginTop:5,textAlign:"center"}}>
                                Please type the verification code sent to <Text style={{color:COLORS.RED}}>0895611826126</Text>
                            </Text>
                        </View>
                        <View style={{width:"80%"}}>
                            <View style={{position:"absolute",width:"100%",height:"100%",alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                                {
                                    dummyPin.map((item,index)=>{
                                        return (
                                            <View
                                                key={index}
                                                style={{width:(80/dummyPin.length)+"%",aspectRatio: 0.8,borderRadius:5,borderWidth:2,borderColor:COLORS.BLUE,backgroundColor:this.state.otp.length > index?COLORS.BLUE:COLORS.LIGHT_GREY,alignItems:"center",justifyContent:"center"}}
                                            >
                                                <Text
                                                    style={{fontSize:16,fontWeight: "bold",color:COLORS.WHITE}}
                                                >
                                                    {this.state.otp.length > index?this.state.otp[index]:""}
                                                </Text>
                                            </View>
                                        )
                                    })
                                }
                            </View>
                        </View>
                        <View style={{flex:1,marginTop:50,paddingHorizontal:20,flexDirection:'row'}}>
                            <Text style={{fontSize:14,textAlign:"center",marginRight:5}}>
                                Didn't get the OTP?
                            </Text>
                            {
                                this.state.countResend === 0?
                                    <TouchableOpacity onPress={() => {
                                        this.props.sendOTP();
                                        this.recount();
                                    }}>
                                        <Text style={{fontSize: 14, textDecorationLine: 'underline', textAlign: "center", fontWeight: "bold"}}>Resend OTP</Text>
                                    </TouchableOpacity>
                                    :
                                    <Text style={{fontSize: 14, textAlign: "center"}}>
                                        Resend Otp in {this.state.countResend}s
                                    </Text>
                            }
                        </View>
                    </View>
                    <View style={{flex:1,flexDirection:"row",flexWrap:"wrap",paddingHorizontal:"5%"}}>
                        {
                            dummyKeyboard.map((item,index)=>{
                               return (
                                   <TouchableOpacity
                                       key={index}
                                       style={{width:"33%",height: "25%",alignItems:"center",justifyContent:"center"}}
                                       onPress={()=>{
                                           this.insertOTP(item.toString());
                                       }}
                                   >
                                       <Text style={{fontSize:24,fontWeight:"bold"}}>{item}</Text>
                                   </TouchableOpacity>
                               )
                            })
                        }
                        <View style={{width:"33%",height: "25%"}}>
                        </View>
                        <TouchableOpacity
                            style={{width:"33%",height: "25%",alignItems:"center",justifyContent:"center"}}
                            onPress={()=>{
                                this.insertOTP("0");
                            }}
                        >
                            <Text style={{fontSize:24,fontWeight:"bold"}}>0</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{width:"33%",height: "25%",alignItems:"center",justifyContent:"center"}}
                            onPress={()=>{
                                this.setState({
                                    otp: this.state.otp.substring(0, this.state.otp.length - 1)
                                })
                            }}
                        >
                            <Icon size={24} type={"material"} name={"backspace"}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </RootView>
        )
    }
}