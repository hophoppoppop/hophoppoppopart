
import React, {Component} from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';
import {observer} from 'mobx-react';
import PropTypes  from 'prop-types';

import Swiper from 'react-native-swiper';
import {Header, Image, RootView} from "../Module";
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";

@observer
export default class ImageViewerPage extends Component{

    static defaultProps = {
        multiple: false,
        photo: null,
        currIndex: 0,
        title:"",
    };

    static propTypes = {
        multiple: PropTypes.bool,
        photo: PropTypes.any,
        currIndex: PropTypes.any,
        title: PropTypes.any
    };

    constructor(props){
        super(props);
    }

    imageRender(uri,index){
        return (
            <View key={index} style={{flex:1,alignItems:"center",justifyContent:"center",backgroundColor:COLORS.BLACK}}>
                <View style={{width:"100%",aspectRatio:1,backgroundColor:COLORS.WHITE}}>
                    <Image style={{width:"100%",height:"100%",resizeMode:"contain"}} source={{uri:uri}}/>
                </View>
            </View>
        )
    }

    render(){
        return (
            <RootView barColor={COLORS.BLACK} barStyle={"light-content"}>
                <Header noBorder backButtonColor={COLORS.WHITE} headerColor={COLORS.BLACK} title={this.props.title} backButtonIcon={"times"} />
                {this.props.multiple && this.props.photo !== null ?
                    <Swiper activeDotColor={COLORS.WHITE} dotColor={COLORS.DARK_GREY} index={this.props.currIndex}>
                        {this.props.photo.map((item, index) =>
                            this.imageRender(item, index)
                        )}
                    </Swiper>
                    :
                    this.imageRender(this.props.photo)
                }
            </RootView>
        )
    }
}