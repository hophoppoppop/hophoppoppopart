
import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView, Dimensions,
    Text,
    TouchableWithoutFeedback,AsyncStorage,
    Slider
} from 'react-native';
import {observer} from 'mobx-react';

import {
    Header,
    Image,
    Loader,
    RootView,
    Icon,
    ConfirmModal,
    PickerModal,
    LoadingModal,
    ComponentModal,
    DatePicker,
    CheckBox,
    DraggableFlatList,
    InputText
} from "../Module";
import COLORS from "../Constant/Colors";
import ImagePicker from "react-native-image-picker";
import ProfileStore from "../Stores/profileStore";
import {PagePop, RouterDirector} from "../Routers/routerManager";
import * as RouterList from "../Routers/routerList";
import {Capitalized, SpaceToUnderLine, UnderLineToSpace} from "../Function/stringFormat";
import RNFS from 'react-native-fs';

import firebase from '@react-native-firebase/app';
import {storage} from '@react-native-firebase/storage';
import {auth} from '@react-native-firebase/auth';
import Collapsible from 'react-native-collapsible';
import PropTypes  from 'prop-types';
import {isEquivalent} from "../Function/objectFormat";
import modalStore from "../Stores/modalStore";
import moment from 'moment';
import loginStore from "../Stores/loginStore";

let {width, height} = Dimensions.get("window");

@observer
export default class EditPage extends Component{

    constructor(props){
        super(props);
        this.state={
            profilePhotoEdit: null,
            profileDataEdit: null,
            deletedData: {
                profile:[],
                work_experience: [],
                education: [],
                skill: [],
            },
            inputValue:{
                key:"",
                value: "",
                index: -1
            },
            inputError:{
                keyError: "",
                valueError: "",
            },
            inputWorkExperience:{
                company:"",
                location:"",
                start_date:"",
                end_date:"",
                description:"",
                index: -1
            },
            inputWorkExperienceError:{
                companyErr:"",
                locationErr:"",
                start_dateErr:"",
                end_dateErr:"",
                descriptionErr:"",
            },
            inputEducation:{
                name:"",
                degree:"",
                field:"",
                start_year:"",
                end_year:"",
                grade:"",
                index: -1
            },
            inputEducationError:{
                nameErr:"",
                degreeErr:"",
                fieldErr:"",
                start_yearErr:"",
                end_yearErr:"",
                gradeErr:"",
            },
            inputSkill:{
                name:"",
                percent:0,
                defaultPercent: 0,
                imageURL:"",
                index: -1
            },
            inputSkillError:{
                nameErr:"",
                percentErr:"",
                imageURLErr:"",
            },
            scrollViewOffset: 0,
            childScrollViewOffset: 0,
            profileViewOffset: 0,
            profileFlatListOffset: 0,
            workExperienceViewOffset: 0,
            workExperienceFlatListOffset: 0,
            educationViewOffset: 0,
            educationFlatListOffset: 0,
            skillViewOffset: 0,
            skillFlatListOffset: 0,
            scrollOffset: 0,
            inputType: "profile",
            deleteCollapse: false,
            isPresent: false
        }
    }

    componentDidMount(){
        if(ProfileStore.profileData.skill === null || ProfileStore.profileData.skill === undefined){
            ProfileStore.profileData.skill = [];
        }
        this.setState({
            profileDataEdit: JSON.parse(JSON.stringify(ProfileStore.profileData))
        });
    }

    uploadImage(url){
        LoadingModal.toggleLoading(true);
        let uploadProfilePhoto = ()=>{
            firebase
                .storage()
                .ref('profile.jpg')
                .putFile(url)
                .on(
                    firebase.storage.TaskEvent.STATE_CHANGED,
                    async snapshot => {
                        if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
                            uploadProfileData(async ()=>{
                                ProfileStore.profilePhoto = await firebase.storage().ref(snapshot.metadata.fullPath).getDownloadURL();
                                LoadingModal.toggleLoading(false);
                                RouterDirector(RouterList.SuccessPage);
                            });
                        }
                    },
                    error => {
                        alert('Sorry, Try again.');
                    }
                );
        };

        let uploadProfileData = (action)=>{
            if (this.state.profileDataEdit !== null && (!isEquivalent(this.state.profileDataEdit, ProfileStore.profileData) || this.state.deletedData.profile.length > 0 || this.state.deletedData.work_experience.length > 0 || this.state.deletedData.education.length > 0))
            {
                let path = RNFS.DocumentDirectoryPath + '/profile.json';
                let removeEmpty = this.state.profileDataEdit;
                removeEmpty.profile = removeEmpty.profile.filter(s=>s.key !== "");
                let removeEmptyWorkExperience = removeEmpty.work_experience.filter(s=>s.company !== "");
                this.state.deletedData.work_experience.map((item)=>{
                    removeEmptyWorkExperience = removeEmptyWorkExperience.filter(s=>s.company !== item.company);
                });
                removeEmpty.work_experience = removeEmptyWorkExperience;
                let removeEmptyEducation = removeEmpty.education.filter(s=>s.name !== "");
                this.state.deletedData.education.map((item)=>{
                    removeEmptyEducation = removeEmptyEducation.filter(s=>s.name !== item.name);
                });
                removeEmpty.education = removeEmptyEducation;
                RNFS.writeFile(path, JSON.stringify(removeEmpty))
                    .then((success) => {
                        firebase
                            .storage()
                            .ref('profile.json')
                            .putFile(path)
                            .on(
                                firebase.storage.TaskEvent.STATE_CHANGED,
                                async snapshot => {
                                    if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
                                        //Set ProfileStore.profileData dengan JSON yang baru
                                        this.setState({
                                            profileDataEdit: removeEmpty
                                        },()=>{
                                            ProfileStore.profileData = removeEmpty;
                                        });
                                        uploadLastUpdated(action);
                                    }
                                },
                                error => {
                                    alert('Sorry, Try again.');
                                }
                            );
                    })
                    .catch((err) => {
                        console.log(err.message);
                    });
            }else{
                uploadLastUpdated(action);
            }
        };

        let uploadLastUpdated = (action)=>{

            let path = RNFS.DocumentDirectoryPath + '/lastUpdated.json';

            RNFS.writeFile(path, JSON.stringify({
                lastUpdated: new Date().getTime()
            }))
                .then((success) => {
                    firebase
                        .storage()
                        .ref('lastUpdated.json')
                        .putFile(path)
                        .on(
                            firebase.storage.TaskEvent.STATE_CHANGED,
                            async snapshot => {
                                if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
                                    action();
                                }
                            },
                            error => {
                                alert('Sorry, Try again.');
                            }
                        );
                })
                .catch((err) => {
                    console.log(err.message);
                });
        };
        if(this.state.profilePhotoEdit !== null && this.state.profilePhotoEdit !== ProfileStore.profilePhoto)
        {
            uploadProfilePhoto();
        }else{
            uploadProfileData(()=>{
                LoadingModal.toggleLoading(false);
                RouterDirector(RouterList.SuccessPage);
            });
        }
    };

    openImagePicker(){
        PickerModal.togglePicker("Select Picture",[{
            name: "Select from gallery",
            value: "imagePicker"
        },{
            name: "Select from camera",
            value: "camera"
        }],(item)=>{
            if(item.value === "imagePicker")
            {
                const options = {
                    storageOptions: {
                        skipBackup: true,
                        path: 'images',
                    },
                };
                ImagePicker.launchImageLibrary(options,(response) => {
                    if (response.didCancel) {
                        console.log('You cancelled image picker');
                    } else if (response.error) {
                        alert('And error occured: ', response.error);
                    } else {
                        this.setState({
                            profilePhotoEdit: response.uri
                        });
                    }
                });
            }else if(item.value === "camera")
            {
                RouterDirector(RouterList.CameraPage, {
                    action: (url) => {
                        PagePop();
                        this.setState({
                            profilePhotoEdit: url
                        })
                    }
                });
            }
        });
        /*const options = {
            title: 'Select Picture',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                this.setState({
                    profilePhotoEdit: response.uri,
                });
            }
        });*/
    }

    renderProfileInput(){
        if(this.state.profileDataEdit !== null)
        {
            return <View onLayout={event => {
                const layout = event.nativeEvent.layout;
                this.setState({
                    profileFlatListOffset: layout.y
                });
            }}>
                <DraggableFlatList
                    hasScrollView
                    parentOffset={this.state.scrollViewOffset + this.state.childScrollViewOffset + this.state.profileViewOffset + this.state.profileFlatListOffset}
                    scrollViewScrollOffset={this.state.scrollOffset}
                    data={this.state.profileDataEdit.profile}
                    keyExtractor={(item, index) => index.toString()}
                    onDataChanged={(data) => {
                        let temp = this.state.profileDataEdit;
                        temp.profile = data;
                        this.setState({
                            profileDataEdit: temp,
                        });
                    }}
                    renderItem={(draggable, {item, index}) => {
                        let tempStore = ProfileStore.profileData.profile;
                        let isDifference = tempStore.length > index && (this.state.deletedData.profile.find(s => s.key === item.key) !== undefined || tempStore[index].key !== item.key || tempStore[index].value !== item.value);
                        let isNew = tempStore.length <= index;
                        let differenceColor = isDifference ? COLORS.BLACK : isNew ? COLORS.BLACK : COLORS.BLACK;
                        return (
                            <View style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
                                {draggable()}
                                <View style={{flex: 1, borderBottomWidth: 1}}>
                                    <TouchableOpacity
                                        style={{
                                            flex: 1,
                                            marginVertical: 10,
                                            justifyContent: "center",
                                            flexDirection: "row"
                                        }}
                                        onPress={() => {
                                            this.openInput(item, index, "profile");
                                        }}
                                    >
                                        <View style={{flex: 1, paddingRight: 20}}>
                                            <Text
                                                style={{fontWeight: "bold", fontSize: 16, color: differenceColor}}>
                                                {item.key === "" ? "--Empty--" : Capitalized(UnderLineToSpace(item.key))}
                                            </Text>
                                            <Text style={{fontSize: 12, color: differenceColor}}>
                                                {item.value === "" ? "--Empty--" : (item.key.toLowerCase().indexOf("date") !== -1 ? moment(item.value).format("DD-MM-YYYY") :item.value)}
                                            </Text>
                                        </View>
                                        {/*{isDifference ? <TouchableOpacity
                                                style={{height: "100%", marginRight: 10, justifyContent: "center"}}
                                                onPress={() => {
                                                    let tempArray = Object.entries(this.state.profileDataEdit);
                                                    let tempFront = tempArray.slice(0, index);
                                                    let tempBack = tempArray.slice(index + 1, tempArray.length);
                                                    tempFront.push(tempStore[index]);
                                                    let temp = {};
                                                    tempFront.concat(tempBack).map((subItem) => {
                                                        temp[subItem[0]] = subItem[1]
                                                    });
                                                    temp.work_experience = this.state.profileDataEdit.work_experience;
                                                    temp.education = this.state.profileDataEdit.education;
                                                    this.setState({
                                                        profileDataEdit: temp
                                                    })
                                                }}
                                            >
                                                <Icon size={24} color={differenceColor} name={"undo"}/>
                                            </TouchableOpacity> : null}*/}
                                        <TouchableOpacity
                                            style={{height: "100%", marginTop: -1, justifyContent: "center"}}
                                            onPress={() => {
                                                if (item.key === "") {
                                                    let temp = this.state.profileDataEdit;
                                                    temp.profile = temp.profile.filter(s=>s.key !== "");
                                                    this.setState({
                                                        profileDataEdit: temp
                                                    })
                                                } else {
                                                    let tempFull = this.state.deletedData;
                                                    let dataFull = this.state.profileDataEdit;
                                                    let temp = tempFull.profile;
                                                    let temporaryItem = item;
                                                    temporaryItem.index = index;
                                                    temp.push(temporaryItem);
                                                    dataFull.profile = dataFull.profile.filter(s=>s.key !== item.key);
                                                    this.setState({
                                                        deletedData: tempFull,
                                                        profileDataEdit: dataFull
                                                    })
                                                }
                                            }}
                                        >
                                            <Icon size={25} color={differenceColor} name={"trash"}/>
                                        </TouchableOpacity>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        )
                    }}
                />
            </View>
        }else{
            return null;
        }
    }

    renderWorkExperienceInput(){
        if(this.state.profileDataEdit !== null && this.state.profileDataEdit.work_experience !== null && this.state.profileDataEdit.work_experience !== undefined)
        {
            return <View onLayout={event => {
                const layout = event.nativeEvent.layout;
                this.setState({
                    workExperienceFlatListOffset: layout.y
                });
            }}>
                <DraggableFlatList
                    hasScrollView
                    parentOffset={this.state.scrollViewOffset + this.state.childScrollViewOffset + this.state.workExperienceViewOffset + this.state.workExperienceFlatListOffset}
                    scrollViewScrollOffset={this.state.scrollOffset}
                    data={this.state.profileDataEdit.work_experience}
                    keyExtractor={(item, index) => index.toString()}
                    onDataChanged={(data) => {
                        let tempArray = this.state.profileDataEdit;
                        tempArray.work_experience = data;
                        this.setState({
                            profileDataEdit: tempArray,
                        });
                    }}
                    renderItem={(draggable, {item, index}) => {
                        if (this.state.deletedData.work_experience.find(s=>s===item) === undefined ) {
                            let tempStore = ProfileStore.profileData.work_experience;

                            let isDifference =  tempStore.length > index && (this.state.deletedData.work_experience.find(s=>s.company.toLowerCase()===item.company.toLowerCase()) !== undefined ||tempStore[index].start_date !== item.start_date || tempStore[index].description !== item.description || tempStore[index].company !== item.company || tempStore[index].location !== item.location || tempStore[index].end_date !== item.end_date);
                            let isNew = tempStore.length <= index;
                            let differenceColor = isDifference?COLORS.BLACK:isNew?COLORS.BLACK:COLORS.BLACK;
                            return (
                                <View style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
                                    {draggable()}
                                    <View style={{flex:1,borderBottomWidth:1}} key={index}>
                                        <TouchableOpacity
                                            style={{flex:1,marginVertical:10,justifyContent:"center",flexDirection:"row"}}
                                            onPress={()=>{
                                                this.openInput(item,index,"work");
                                            }}
                                        >
                                            <View style={{flex:1,paddingRight:20}}>
                                                <Text style={{fontWeight:"bold",color:differenceColor}}>
                                                    {item.company === ""?"--Empty--":item.company}
                                                </Text>
                                                <Text style={{fontWeight:"bold",color:differenceColor}}>
                                                    {moment(item.start_date).format("MMM YYYY")} - {item.end_date === "-"?"Present":moment(item.end_date).format("MMM YYYY")}
                                                </Text>
                                                <Text style={{color:differenceColor}}>
                                                    {item.description === ""?"--Empty--":item.description}
                                                </Text>
                                                <Text style={{color:differenceColor}} numberOfLines={1} ellipsizeMode={"tail"}>
                                                    {item.location === ""?"--Empty--":item.location}
                                                </Text>
                                            </View>
                                            {/*{isDifference?<TouchableOpacity
                                                style={{height:"100%",marginRight:10,justifyContent:"center"}}
                                                onPress={()=>{
                                                    let fullTemp = this.state.profileDataEdit;
                                                    let tempArray = fullTemp.work_experience;
                                                    let tempFront = tempArray.slice(0,index);
                                                    let tempBack = tempArray.slice(index+1,tempArray.length);
                                                    tempFront.push(tempStore[index]);
                                                    fullTemp.work_experience = tempFront.concat(tempBack);
                                                    this.setState({
                                                        profileDataEdit:fullTemp
                                                    })
                                                }}
                                            >
                                                <Icon size={24} color={differenceColor} name={"undo"} />
                                            </TouchableOpacity>:null}*/}
                                            <TouchableOpacity
                                                style={{height:"100%",marginTop:-1,justifyContent:"center"}}
                                                onPress={()=>{
                                                    if (item.company === "") {
                                                        let temp = this.state.profileDataEdit;
                                                        temp.work_experience = temp.work_experience.filter(s=>s.company !== "");;
                                                        this.setState({
                                                            profileDataEdit: temp
                                                        })
                                                    } else {
                                                        let tempFull = this.state.deletedData;
                                                        let dataFull = this.state.profileDataEdit;
                                                        let temp= tempFull.work_experience;
                                                        let temporaryItem = item;
                                                        temporaryItem.index = index;
                                                        temp.push(temporaryItem);
                                                        dataFull.work_experience = dataFull.work_experience.filter(s=>s.company !== item.company);
                                                        this.setState({
                                                            deletedData: tempFull,
                                                            profileDataEdit: dataFull
                                                        })
                                                    }
                                                }}
                                            >
                                                <Icon size={25} color={differenceColor} name={"trash"} />
                                            </TouchableOpacity>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        } else {
                            return null;
                        }
                    }}
                />
            </View>
        }else{
            return null;
        }
    }

    renderEducationInput(){
        if(this.state.profileDataEdit !== null && this.state.profileDataEdit.education !== null && this.state.profileDataEdit.education !== undefined)
        {
            return <View onLayout={event => {
                const layout = event.nativeEvent.layout;
                this.setState({
                    educationFlatListOffset: layout.y
                });
            }}>
                <DraggableFlatList
                    hasScrollView
                    parentOffset={this.state.scrollViewOffset + this.state.childScrollViewOffset + this.state.educationViewOffset + this.state.educationFlatListOffset}
                    scrollViewScrollOffset={this.state.scrollOffset}
                    data={this.state.profileDataEdit.education}
                    keyExtractor={(item, index) => index.toString()}
                    onDataChanged={(data) => {
                        let tempArray = this.state.profileDataEdit;
                        tempArray.education = data;
                        this.setState({
                            profileDataEdit: tempArray,
                        });
                    }}
                    renderItem={(draggable, {item, index}) => {
                        if (this.state.deletedData.education.find(s=>s===item) === undefined ) {
                            let tempStore = ProfileStore.profileData.education;

                            let isDifference =  tempStore.length > index && (this.state.deletedData.education.find(s=>s.name.toLowerCase()===item.name.toLowerCase()) !== undefined ||tempStore[index].degree !== item.degree || tempStore[index].field !== item.field || tempStore[index].start_year !== item.start_year || tempStore[index].end_year !== item.end_year || tempStore[index].grade !== item.grade);
                            let isNew = tempStore.length <= index;
                            let differenceColor = isDifference?COLORS.BLACK:isNew?COLORS.BLACK:COLORS.BLACK;
                            return (
                                <View style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
                                    {draggable()}
                                    <View style={{flex:1,borderBottomWidth:1}} key={index}>
                                        <TouchableOpacity
                                            style={{flex:1,marginVertical:10,justifyContent:"center",flexDirection:"row"}}
                                            onPress={()=>{
                                                this.openInput(item,index,"education");
                                            }}
                                        >
                                            <View style={{flex:1,paddingRight:20}}>
                                                <Text style={{fontWeight:"bold",color:differenceColor}}>
                                                    {item.name === ""?"--Empty--":item.name}
                                                </Text>
                                                <Text style={{fontWeight:"bold",color:differenceColor}}>
                                                    {moment(item.start_year).format("YYYY")} - {item.end_year === "-"?"Present":moment(item.end_year).format("YYYY")}
                                                </Text>
                                                <Text style={{color:differenceColor}}>
                                                    {item.degree === ""?"--Empty--":item.degree}
                                                </Text>
                                                <Text style={{color:differenceColor}} numberOfLines={1} ellipsizeMode={"tail"}>
                                                    {item.field === ""?"--Empty--":item.field}
                                                </Text>
                                                <Text style={{color:differenceColor}} numberOfLines={1} ellipsizeMode={"tail"}>
                                                    {item.grade === ""?"--Empty--":item.grade+" GPA"}
                                                </Text>
                                            </View>
                                            {/*{isDifference?<TouchableOpacity
                                                style={{height:"100%",marginRight:10,justifyContent:"center"}}
                                                onPress={()=>{
                                                    let fullTemp = this.state.profileDataEdit;
                                                    let tempArray = fullTemp.work_experience;
                                                    let tempFront = tempArray.slice(0,index);
                                                    let tempBack = tempArray.slice(index+1,tempArray.length);
                                                    tempFront.push(tempStore[index]);
                                                    fullTemp.work_experience = tempFront.concat(tempBack);
                                                    this.setState({
                                                        profileDataEdit:fullTemp
                                                    })
                                                }}
                                            >
                                                <Icon size={24} color={differenceColor} name={"undo"} />
                                            </TouchableOpacity>:null}*/}
                                            <TouchableOpacity
                                                style={{height:"100%",marginTop:-1,justifyContent:"center"}}
                                                onPress={()=>{
                                                    if (item.name === "") {
                                                        let temp = this.state.profileDataEdit;
                                                        temp.education = temp.education.filter(s=>s.name !== "");;
                                                        this.setState({
                                                            profileDataEdit: temp
                                                        })
                                                    } else {
                                                        let tempFull = this.state.deletedData;
                                                        let dataFull = this.state.profileDataEdit;
                                                        let temp = tempFull.education;
                                                        let temporaryItem = item;
                                                        temporaryItem.index = index;
                                                        temp.push(temporaryItem);
                                                        dataFull.education = dataFull.education.filter(s=>s.name !== item.name);
                                                        this.setState({
                                                            deletedData: tempFull,
                                                            profileDataEdit: dataFull
                                                        })
                                                    }
                                                }}
                                            >
                                                <Icon size={25} color={differenceColor} name={"trash"} />
                                            </TouchableOpacity>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        } else {
                            return null;
                        }
                    }}
                />
            </View>
        }else{
            return null;
        }
    }


    renderSkillInput(){
        if(this.state.profileDataEdit !== null && this.state.profileDataEdit.skill !== null && this.state.profileDataEdit.skill !== undefined)
        {
            return <View onLayout={event => {
                const layout = event.nativeEvent.layout;
                this.setState({
                    skillFlatListOffset: layout.y
                });
            }}>
                <DraggableFlatList
                    hasScrollView
                    parentOffset={this.state.scrollViewOffset + this.state.childScrollViewOffset + this.state.skillViewOffset + this.state.skillFlatListOffset}
                    scrollViewScrollOffset={this.state.scrollOffset}
                    data={this.state.profileDataEdit.skill}
                    keyExtractor={(item, index) => index.toString()}
                    onDataChanged={(data) => {
                        let tempArray = this.state.profileDataEdit;
                        tempArray.skill = data;
                        this.setState({
                            profileDataEdit: tempArray,
                        });
                    }}
                    renderItem={(draggable, {item, index}) => {
                        if (this.state.deletedData.education.find(s=>s===item) === undefined ) {
                            let tempStore = ProfileStore.profileData.skill;

                            let isDifference =  tempStore.length > index && (this.state.deletedData.skill.find(s=>s.name.toLowerCase()===item.name.toLowerCase()) !== undefined ||tempStore[index].percent !== item.percent || tempStore[index].imageURL !== item.imageURL);
                            let isNew = tempStore.length <= index;
                            let differenceColor = isDifference?COLORS.BLACK:isNew?COLORS.BLACK:COLORS.BLACK;
                            return (
                                <View style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
                                    {draggable()}
                                    <View style={{flex:1,borderBottomWidth:1}} key={index}>
                                        <TouchableOpacity
                                            style={{flex:1,marginVertical:10,justifyContent:"center",flexDirection:"row"}}
                                            onPress={()=>{
                                                this.openInput(item,index,"skill");
                                            }}
                                        >
                                            <View style={{flex:1,paddingRight:20}}>
                                                <Text style={{fontWeight:"bold",color:differenceColor}}>
                                                    {item.name === ""?"--Empty--":item.name}
                                                </Text>
                                                <Text style={{color:differenceColor}}>
                                                    {item.percent === ""?"--Empty--":item.percent+"%"}
                                                </Text>
                                            </View>
                                            {/*{isDifference?<TouchableOpacity
                                                style={{height:"100%",marginRight:10,justifyContent:"center"}}
                                                onPress={()=>{
                                                    let fullTemp = this.state.profileDataEdit;
                                                    let tempArray = fullTemp.work_experience;
                                                    let tempFront = tempArray.slice(0,index);
                                                    let tempBack = tempArray.slice(index+1,tempArray.length);
                                                    tempFront.push(tempStore[index]);
                                                    fullTemp.work_experience = tempFront.concat(tempBack);
                                                    this.setState({
                                                        profileDataEdit:fullTemp
                                                    })
                                                }}
                                            >
                                                <Icon size={24} color={differenceColor} name={"undo"} />
                                            </TouchableOpacity>:null}*/}
                                            <TouchableOpacity
                                                style={{height:"100%",marginTop:-1,justifyContent:"center"}}
                                                onPress={()=>{
                                                    if (item.name === "") {
                                                        let temp = this.state.profileDataEdit;
                                                        temp.skill = temp.skill.filter(s=>s.name !== "");;
                                                        this.setState({
                                                            profileDataEdit: temp
                                                        })
                                                    } else {
                                                        let tempFull = this.state.deletedData;
                                                        let dataFull = this.state.profileDataEdit;
                                                        let temp = tempFull.skill;
                                                        let temporaryItem = item;
                                                        temporaryItem.index = index;
                                                        temp.push(temporaryItem);
                                                        dataFull.skill = dataFull.skill.filter(s=>s.name !== item.name);
                                                        this.setState({
                                                            deletedData: tempFull,
                                                            profileDataEdit: dataFull
                                                        })
                                                    }
                                                }}
                                            >
                                                <Icon size={25} color={differenceColor} name={"trash"} />
                                            </TouchableOpacity>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        } else {
                            return null;
                        }
                    }}
                />
            </View>
        }else{
            return null;
        }
    }

    confirmAction(){
        if (
            this.state.deletedData.profile.length > 0 ||
            this.state.deletedData.education.length > 0 ||
            this.state.deletedData.work_experience.length > 0 ||
            (this.state.profileDataEdit !== null && JSON.stringify(this.state.profileDataEdit) !== JSON.stringify(ProfileStore.profileData)) ||
            (this.state.profilePhotoEdit    !== null && this.state.profilePhotoEdit !== ProfileStore.profilePhoto))
        {
            return ()=>{
                ConfirmModal.toggleConfirmation("Are you sure you want to update this profile?",()=>{
                    this.uploadImage(this.state.profilePhotoEdit);
                })
            };
        }else{
            return null;
        }
    }

    openInput(item,index,type){
        /*
            ----TYPE----
            profile,
            work,
            education
        * */
        if(type === "profile")
        {
            this.setState({
                inputValue:{
                    key: item.key,
                    value:item.value,
                    index:index
                },
                inputError:{
                    keyError: "",
                    valueError: ""
                },
                inputType: type
            },()=>{
                this.modal.toggleModal(true);
            })
        }else if(type === "work")
        {
            this.setState({
                inputWorkExperience:{
                    company:item.company,
                    location:item.location,
                    start_date:item.start_date,
                    end_date:item.end_date,
                    description:item.description,
                    index: index
                },
                inputWorkExperienceError:{
                    companyErr:"",
                    locationErr:"",
                    start_dateErr:"",
                    end_dateErr:"",
                    descriptionErr:"",
                },
                isPresent: item.end_date === "-",
                inputType: type
            },()=>{
                this.modal.toggleModal(true);
            })
        }else if(type === "education")
        {
            this.setState({
                inputEducation:{
                    name:item.name,
                    degree:item.degree,
                    field: item.field,
                    start_year:item.start_year,
                    end_year:item.end_year,
                    grade:item.grade,
                    index: index
                },
                inputEducationError:{
                    nameErr:"",
                    degreeErr:"",
                    fieldErr:"",
                    start_yearErr:"",
                    end_yearErr:"",
                    gradeErr:"",
                },
                isPresent: item.end_year === "-",
                inputType: type
            },()=>{
                this.modal.toggleModal(true);
            })
        }else if(type === "skill")
        {
            this.setState({
                inputSkill:{
                    name:item.name,
                    percent:item.percent,
                    defaultPercent: item.percent,
                    imageURL:item.imageURL,
                    index: index
                },
                inputEducationError:{
                    nameErr:"",
                    percentErr:"",
                    imageURLErr:"",
                },
                inputType: type
            },()=>{
                this.modal.toggleModal(true);
            })
        }
    }

    renderInputModal(){
        if(this.state.inputType === "profile")
        {
            return (

                <View style={{width:"100%",paddingTop:10}}>
                    <View style={{width:"100%",paddingHorizontal:15,marginBottom:10}}>
                        <InputText
                            title={"Key"}
                            value={Capitalized(UnderLineToSpace(this.state.inputValue.key))}
                            error={this.state.inputError.keyError}
                            action={(text)=>{
                                let temp = this.state.inputValue;
                                let tempError = this.state.inputError;
                                temp.key = SpaceToUnderLine(text.replace(/[^a-zA-Z_ ]/gi, "").toLowerCase());
                                let isExists = this.state.profileDataEdit.profile.find(s=>s.key === temp.key) !== undefined && (this.state.inputValue.index === -1 || this.state.profileDataEdit.profile[this.state.inputValue.index].key !== temp.key) && this.state.deletedData.profile.find(s=>s.key === temp.key) === undefined;
                                if(isExists)
                                {
                                    tempError.keyError = "Key already available!";
                                }else{
                                    tempError.keyError = "";
                                }
                                if(temp.key.toLowerCase().indexOf("date") !== -1)
                                {
                                    temp.value = new Date().getTime();
                                }else{
                                    if(!isNaN(temp.value))
                                    {
                                        temp.value = "";
                                    }
                                }
                                this.setState({
                                    inputValue: temp,
                                    inputError: tempError
                                })
                            }}
                        />
                        <InputText
                            title={"Value"}
                            isDate={this.state.inputValue.key.toLowerCase().indexOf("date") !== -1}
                            value={this.state.inputValue.value}
                            error={this.state.inputError.valueError}
                            action={(text)=>{
                                let temp = this.state.inputValue;
                                temp.value = text;
                                this.setState({
                                    inputValue: temp
                                })
                            }}
                        />
                    </View>
                    <View style={{width:"100%",height:40,flexDirection:"row"}}>
                        <TouchableOpacity
                            disabled={this.state.inputError.keyError !== "" || this.state.inputError.valueError !== ""}
                            style={{width:"50%",height:"100%", backgroundColor:this.state.inputError.keyError !== "" || this.state.inputError.valueError !== ""?COLORS.DARK_GREY:COLORS.BLUE,alignItems:"center",justifyContent:"center"}}
                            onPress={()=>{
                                let tempArray = this.state.profileDataEdit;
                                let tempProfile = tempArray.profile;
                                let index = this.state.inputValue.index;
                                if (index === -1 || this.state.inputValue.key !== tempProfile[index].key || this.state.inputValue.value !== tempProfile[index].value) {
                                    let tempFront = tempProfile;
                                    let tempBack = [];
                                    if(index !== -1)
                                    {
                                        tempFront = tempProfile.slice(0,index);
                                        tempBack = tempProfile.slice(index+1,tempProfile.length);
                                    }
                                    tempFront.push({key:this.state.inputValue.key,value:this.state.inputValue.value});
                                    let temp = tempFront.concat(tempBack);
                                    let fullDeleted = this.state.deletedData;
                                    let tempDeleted = fullDeleted.profile;
                                    let isDeleted = tempDeleted.find(s=>s === this.state.inputValue.key) !== undefined;
                                    if(isDeleted)
                                    {
                                        tempDeleted = tempDeleted.filter(s=> s!==this.state.inputValue.key);
                                        fullDeleted.profile = tempDeleted;
                                    }
                                    tempArray.profile = temp;
                                    this.setState({
                                        profileDataEdit: tempArray,
                                        deletedData: fullDeleted
                                    })
                                }
                                this.modal.toggleModal(false);
                            }}
                        >
                            <Text style={{color:COLORS.WHITE}}>Confirm</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{width:"50%",height:"100%", backgroundColor:COLORS.LIGHT_GREY,alignItems:"center",justifyContent:"center"}}
                            onPress={()=>{
                                this.modal.toggleModal(false);
                            }}>
                            <Text style={{color:COLORS.BLACK}}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }else if(this.state.inputType === "work")
        {
            return (
                <View style={{width:"100%",paddingTop:10}}>
                    <View style={{width:"100%",paddingHorizontal:15,marginBottom:10}}>
                        <InputText
                            title={"Company"}
                            value={this.state.inputWorkExperience.company}
                            error={this.state.inputWorkExperienceError.companyErr}
                            action={(text)=>{
                                let temp = this.state.inputWorkExperience;
                                temp.company = text;
                                let tempError = this.state.inputWorkExperienceError;
                                let index = this.state.inputWorkExperience.index;
                                let isExists = this.state.profileDataEdit.work_experience.find(s=>s.company.toLowerCase() === temp.company.toLowerCase()) !== undefined && (this.state.inputWorkExperience.index === -1 || this.state.profileDataEdit.work_experience[index].company !== temp.company) && this.state.deletedData.work_experience.find(s=>s.company.toLowerCase() === temp.company.toLowerCase()) === undefined;
                                if(isExists)
                                {
                                    tempError.companyErr = "Company name already available!";
                                }else{
                                    tempError.companyErr = "";
                                }
                                this.setState({
                                    inputWorkExperience: temp,
                                    inputWorkExperienceError: tempError
                                })
                            }}
                        />
                        <View style={{width:"100%",flexDirection:"row",}}>
                            <View style={{width:"50%",paddingRight:15}}>
                                <Text style={{marginBottom:5,fontSize:16,fontWeight:"bold"}}>
                                    Start Date
                                </Text>
                                <DatePicker format={"MMM YYYY"} customStyles={{dateInput:{borderWidth: 0,},}} date={this.state.inputWorkExperience.start_date} dayLess style={{width:"100%",borderWidth:1,borderColor:COLORS.GREY,borderRadius:10,paddingVertical:13}}
                                            onDateChange={(date)=>{
                                                let temp = this.state.inputWorkExperience;
                                                temp.start_date = date;
                                                this.setState({
                                                    inputWorkExperience: temp,
                                                });
                                            }}
                                />
                                <View style={{justifyContent:"center",alignSelf:"flex-end",marginTop:25}}>
                                    <Text style={{color:COLORS.RED}}>
                                        {this.state.inputWorkExperienceError.start_dateErr}
                                    </Text>
                                </View>
                            </View>
                            <View style={{width:"50%",paddingLeft:15}}>
                                <Text style={{marginBottom:5,fontSize:16,fontWeight:"bold"}}>
                                    End Date
                                </Text>
                                {this.state.inputWorkExperience.end_date !== "-"?<DatePicker format={"MMM YYYY"} customStyles={{dateInput:{borderWidth: 0,},}} date={this.state.inputWorkExperience.end_date} dayLess style={{width:"100%",borderWidth:1,borderColor:COLORS.GREY,borderRadius:10,paddingVertical:13}}
                                            onDateChange={(date)=>{
                                                let temp = this.state.inputWorkExperience;
                                                temp.end_date = date;
                                                this.setState({
                                                    inputWorkExperience: temp,
                                                });
                                            }}
                                />:<View style={{width:"100%",borderWidth:1,borderColor:COLORS.GREY,borderRadius:10,paddingVertical:13,alignItems:"center",justifyContent:"center"}}>
                                    <Text>Present</Text>
                                </View>}
                                <View style={{width:"100%",marginTop:5,alignItems:"center",flexDirection:"row",justifyContent:"flex-end"}}>
                                    <CheckBox style={{marginRight:5}} size={20} active={this.state.isPresent} onValueChanged={()=>{
                                        this.setState({
                                            isPresent: !this.state.isPresent
                                        },()=>{
                                            let temp = this.state.inputWorkExperience;
                                            if(!this.state.isPresent)
                                            {
                                                temp.end_date = new Date();
                                            }else{
                                                temp.end_date = "-";
                                            }
                                            this.setState({
                                                inputWorkExperience: temp,
                                            });
                                        })
                                    }}/>
                                    <Text>Present</Text>
                                </View>
                                <View style={{justifyContent:"center",alignSelf:"flex-end"}}>
                                    <Text style={{color:COLORS.RED}}>
                                        {this.state.inputWorkExperienceError.end_dateErr}
                                    </Text>
                                </View>
                            </View>
                        </View>
                        <InputText
                            title={"Description"}
                            value={this.state.inputWorkExperience.description}
                            error={this.state.inputWorkExperienceError.descriptionErr}
                            action={(text)=>{
                                let temp = this.state.inputWorkExperience;
                                temp.description = text;
                                this.setState({
                                    inputWorkExperience: temp
                                })
                            }}
                        />
                        <InputText
                            title={"Address"}
                            multiline
                            maxLength={120}
                            value={this.state.inputWorkExperience.location}
                            error={this.state.inputWorkExperienceError.locationErr}
                            action={(text)=>{
                                let temp = this.state.inputWorkExperience;
                                temp.location = text;
                                this.setState({
                                    inputWorkExperience: temp
                                })
                            }}
                        />
                    </View>
                    <View style={{width:"100%",height:40,flexDirection:"row"}}>
                        <TouchableOpacity
                            disabled={this.state.inputWorkExperienceError.companyErr !== "" || this.state.inputWorkExperienceError.descriptionErr !== "" || this.state.inputWorkExperienceError.locationErr !== ""}
                            style={{width:"50%",height:"100%", backgroundColor:(this.state.inputWorkExperienceError.companyErr !== "" || this.state.inputWorkExperienceError.descriptionErr !== "" || this.state.inputWorkExperienceError.locationErr !== "")?COLORS.DARK_GREY:COLORS.BLUE,alignItems:"center",justifyContent:"center"}}
                            onPress={()=>{
                                let fullTemp = this.state.profileDataEdit;
                                let tempArray = fullTemp.work_experience;
                                let tempInputObject = Object.assign({},this.state.inputWorkExperience);
                                delete tempInputObject.index;
                                let index = this.state.inputWorkExperience.index;
                                if (index === -1 || !isEquivalent(tempInputObject,tempArray[index])) {
                                    let fullDeleted = this.state.deletedData;
                                    let tempDeleted = fullDeleted.work_experience;
                                    let isDeleted = tempDeleted.find(s=> s.company.toLowerCase() === this.state.inputWorkExperience.company.toLowerCase()) !== undefined;
                                    let realIndex = index;
                                    if(isDeleted)
                                    {
                                        tempDeleted = tempDeleted.filter(s=> s.company.toLowerCase() !==this.state.inputWorkExperience.company.toLowerCase());
                                        realIndex = tempArray.findIndex(s=>s.company.toLowerCase() === this.state.inputWorkExperience.company.toLowerCase());
                                        fullDeleted.work_experience = tempDeleted;
                                    }
                                    let tempFront = tempArray;
                                    let tempBack = [];
                                    if(realIndex !== -1)
                                    {
                                        tempFront = tempArray.slice(0,realIndex);
                                        tempBack = tempArray.slice(realIndex+1,tempArray.length);
                                    }
                                    tempFront.push(tempInputObject);
                                    let temp = tempFront.concat(tempBack);
                                    fullTemp.work_experience = temp;
                                    this.setState({
                                        profileDataEdit: fullTemp,
                                        deletedData: fullDeleted
                                    })
                                }
                                this.modal.toggleModal(false);
                            }}
                        >
                            <Text style={{color:COLORS.WHITE}}>Confirm</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{width:"50%",height:"100%", backgroundColor:COLORS.LIGHT_GREY,alignItems:"center",justifyContent:"center"}}
                            onPress={()=>{
                                this.modal.toggleModal(false);
                            }}>
                            <Text style={{color:COLORS.BLACK}}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }else if(this.state.inputType === "education") {
            return (
                <View style={{width:"100%",paddingTop:10}}>
                    <View style={{width:"100%",paddingHorizontal:15,marginBottom:10}}>
                        <InputText
                            title={"University Name"}
                            value={this.state.inputEducation.name}
                            error={this.state.inputEducationError.nameErr}
                            action={(text)=>{
                                let temp = this.state.inputEducation;
                                temp.name = text;
                                let tempError = this.state.inputEducationError;
                                let index = this.state.inputEducation.index;
                                let isExists = this.state.profileDataEdit.education.find(s=>s.name.toLowerCase() === temp.name.toLowerCase()) !== undefined && (this.state.inputEducation.index === -1 || this.state.profileDataEdit.education[index].name !== temp.name) && this.state.deletedData.education.find(s=>s.name.toLowerCase() === temp.name.toLowerCase()) === undefined;
                                if(isExists)
                                {
                                    tempError.nameErr = "University name already available!";
                                }else{
                                    tempError.nameErr = "";
                                }
                                this.setState({
                                    inputEducation: temp,
                                    inputEducationError: tempError
                                })
                            }}
                        />
                        <View style={{width:"100%",flexDirection:"row",}}>
                            <View style={{width:"50%",paddingRight:15}}>
                                <Text style={{marginBottom:5,fontSize:16,fontWeight:"bold"}}>
                                    Start Year
                                </Text>
                                <DatePicker format={"YYYY"} customStyles={{dateInput:{borderWidth: 0,},}} date={this.state.inputEducation.start_year} yearOnly style={{width:"100%",borderWidth:1,borderColor:COLORS.GREY,borderRadius:10,}}
                                            onDateChange={(date)=>{
                                                let temp = this.state.inputEducation;
                                                temp.start_year = date;
                                                this.setState({
                                                    inputEducation: temp,
                                                });
                                            }}
                                />
                                <View style={{justifyContent:"center",alignSelf:"flex-end",marginTop:25}}>
                                    <Text style={{color:COLORS.RED}}>
                                        {this.state.inputEducationError.start_yearErr}
                                    </Text>
                                </View>
                            </View>
                            <View style={{width:"50%",paddingLeft:15}}>
                                <Text style={{marginBottom:5,fontSize:16,fontWeight:"bold"}}>
                                    End Date
                                </Text>
                                {this.state.inputEducation.end_year !== "-"?<DatePicker format={"YYYY"} customStyles={{dateInput:{borderWidth: 0,},}} date={this.state.inputEducation.end_year} yearOnly style={{width:"100%",borderWidth:1,borderColor:COLORS.GREY,borderRadius:10,paddingVertical:13}}
                                                                                             onDateChange={(date)=>{
                                                                                                 let temp = this.state.inputEducation;
                                                                                                 temp.end_year = date;
                                                                                                 this.setState({
                                                                                                     inputEducation: temp,
                                                                                                 });
                                                                                             }}
                                />:<View style={{width:"100%",borderWidth:1,borderColor:COLORS.GREY,borderRadius:10,paddingVertical:13,alignItems:"center",justifyContent:"center"}}>
                                    <Text>Present</Text>
                                </View>}
                                <View style={{width:"100%",marginTop:5,alignItems:"center",flexDirection:"row",justifyContent:"flex-end"}}>
                                    <CheckBox style={{marginRight:5}} size={20} active={this.state.isPresent} onValueChanged={()=>{
                                        this.setState({
                                            isPresent: !this.state.isPresent
                                        },()=>{
                                            let temp = this.state.inputEducation;
                                            if(!this.state.isPresent)
                                            {
                                                temp.end_year = new Date();
                                            }else{
                                                temp.end_year = "-";
                                            }
                                            this.setState({
                                                inputEducation: temp,
                                            });
                                        })
                                    }}/>
                                    <Text>Present</Text>
                                </View>
                                <View style={{justifyContent:"center",alignSelf:"flex-end"}}>
                                    <Text style={{color:COLORS.RED}}>
                                        {this.state.inputEducationError.end_yearErr}
                                    </Text>
                                </View>
                            </View>
                        </View>
                        <InputText
                            title={"Degree"}
                            value={this.state.inputEducation.degree}
                            error={this.state.inputEducationError.degreeErr}
                            action={(text)=>{
                                let temp = this.state.inputEducation;
                                temp.degree = text;
                                this.setState({
                                    inputEducation: temp
                                })
                            }}
                        />
                        <InputText
                            title={"Field"}
                            value={this.state.inputEducation.field}
                            error={this.state.inputEducationError.fieldErr}
                            action={(text)=>{
                                let temp = this.state.inputEducation;
                                temp.field = text;
                                this.setState({
                                    inputEducation: temp
                                })
                            }}
                        />
                        <InputText
                            title={"Grade"}
                            value={this.state.inputEducation.grade}
                            error={this.state.inputEducationError.gradeErr}
                            action={(text)=>{
                                let temp = this.state.inputEducation;
                                temp.grade = text;
                                this.setState({
                                    inputEducation: temp
                                })
                            }}
                        />
                    </View>
                    <View style={{width:"100%",height:40,flexDirection:"row"}}>
                        <TouchableOpacity
                            disabled={this.state.inputEducationError.fieldErr !== "" || this.state.inputEducationError.degreeErr !== "" || this.state.inputEducationError.gradeErr !== "" || this.state.inputEducationError.nameErr !== ""}
                            style={{width:"50%",height:"100%", backgroundColor:(this.state.inputEducationError.fieldErr !== "" || this.state.inputEducationError.degreeErr !== "" || this.state.inputEducationError.gradeErr !== "" || this.state.inputEducationError.nameErr !== "")?COLORS.DARK_GREY:COLORS.BLUE,alignItems:"center",justifyContent:"center"}}
                            onPress={()=>{
                                let fullTemp = this.state.profileDataEdit;
                                let tempArray = fullTemp.education;
                                let tempInputObject = Object.assign({},this.state.inputEducation);
                                delete tempInputObject.index;
                                let index = this.state.inputEducation.index;
                                if (index === -1 || !isEquivalent(tempInputObject,tempArray[index])) {
                                    let fullDeleted = this.state.deletedData;
                                    let tempDeleted = fullDeleted.education;
                                    let isDeleted = tempDeleted.find(s=> s.name.toLowerCase() === this.state.inputEducation.name.toLowerCase()) !== undefined;
                                    let realIndex = index;
                                    if(isDeleted)
                                    {
                                        tempDeleted = tempDeleted.filter(s=> s.name.toLowerCase() !==this.state.inputEducation.name.toLowerCase());
                                        realIndex = tempArray.findIndex(s=>s.name.toLowerCase() === this.state.inputEducation.name.toLowerCase());
                                        fullDeleted.education = tempDeleted;
                                    }
                                    let tempFront = tempArray;
                                    let tempBack = [];
                                    if(realIndex !== -1)
                                    {
                                        tempFront = tempArray.slice(0,realIndex);
                                        tempBack = tempArray.slice(realIndex+1,tempArray.length);
                                    }
                                    tempFront.push(tempInputObject);
                                    let temp = tempFront.concat(tempBack);
                                    fullTemp.education = temp;
                                    this.setState({
                                        profileDataEdit: fullTemp,
                                        deletedData: fullDeleted
                                    })
                                }
                                this.modal.toggleModal(false);
                            }}
                        >
                            <Text style={{color:COLORS.WHITE}}>Confirm</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{width:"50%",height:"100%", backgroundColor:COLORS.LIGHT_GREY,alignItems:"center",justifyContent:"center"}}
                            onPress={()=>{
                                this.modal.toggleModal(false);
                            }}>
                            <Text style={{color:COLORS.BLACK}}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }else if(this.state.inputType === "skill"){
            return (
                <View style={{width:"100%",paddingTop:10}}>
                    <View style={{width:"100%",paddingHorizontal:15,marginBottom:10}}>
                        <InputText
                            title={"Skill Name"}
                            value={this.state.inputSkill.name}
                            error={this.state.inputSkillError.nameErr}
                            action={(text)=>{
                                let temp = this.state.inputSkill;
                                temp.name = text;
                                let tempError = this.state.inputSkillError;
                                let index = this.state.inputSkill.index;
                                let isExists = this.state.profileDataEdit.skill.find(s=>s.name.toLowerCase() === temp.name.toLowerCase()) !== undefined && (this.state.inputSkill.index === -1 || this.state.profileDataEdit.skill[index].name !== temp.name) && this.state.deletedData.skill.find(s=>s.name.toLowerCase() === temp.name.toLowerCase()) === undefined;
                                if(isExists)
                                {
                                    tempError.nameErr = "Skill already available!";
                                }else{
                                    tempError.nameErr = "";
                                }
                                this.setState({
                                    inputSkill: temp,
                                    inputSkillError: tempError
                                })
                            }}
                        />
                        {/*<InputText
                            title={"Percent"}
                            value={this.state.inputSkill.percent}
                            error={this.state.inputSkillError.percentErr}
                            action={(text)=>{
                                let temp = this.state.inputSkill;
                                temp.percent = text;
                                this.setState({
                                    inputSkill: temp
                                })
                            }}
                        />*/}
                        <View style={{width:"100%",paddingRight:15}}>
                            <Text style={{marginBottom:5,fontSize:16,fontWeight:"bold"}}>
                                Amount
                            </Text>
                            <View
                                style={{
                                    flexDirection:"row",
                                    alignItems:"center"
                                }}
                            >
                                <View
                                    style={{
                                        flex:1,
                                        alignItems:"center",
                                        justifyContent:"center"
                                    }}
                                >
                                    <Slider
                                        style={{
                                            width:"100%"
                                        }}
                                        value={(this.state.inputSkill.defaultPercent/100)}
                                        onValueChange={(value)=>{
                                            let temp = this.state.inputSkill;
                                            temp.percent = Math.round(value*100).toString();
                                            this.setState({
                                                inputSkill: temp
                                            })
                                        }}
                                    />
                                </View>
                                <View
                                    style={{
                                        height:20,
                                        width:60,
                                        borderRadius:5,
                                        alignItems:"center",
                                        justifyContent:"center",
                                        backgroundColor:COLORS.BLUE
                                    }}
                                >
                                    <Text
                                        style={{
                                            color:COLORS.WHITE
                                        }}
                                    >{this.state.inputSkill.percent}%</Text>
                                </View>
                            </View>
                            <View style={{justifyContent:"center",alignSelf:"flex-end"}}>
                                <Text style={{color:COLORS.RED}}>
                                    {this.state.inputSkillError.percentErr}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{width:"100%",height:40,flexDirection:"row"}}>
                        <TouchableOpacity
                            disabled={this.state.inputSkillError.nameErr !== "" || this.state.inputSkillError.percentErr !== "" || this.state.inputSkillError.imageURLErr !== ""}
                            style={{width:"50%",height:"100%", backgroundColor:(this.state.inputSkillError.nameErr !== "" || this.state.inputSkillError.percentErr !== "" || this.state.inputSkillError.imageURLErr !== "")?COLORS.DARK_GREY:COLORS.BLUE,alignItems:"center",justifyContent:"center"}}
                            onPress={()=>{
                                let fullTemp = this.state.profileDataEdit;
                                let tempArray = fullTemp.skill;
                                let tempInputObject = Object.assign({},this.state.inputSkill);
                                delete tempInputObject.index;
                                let index = this.state.inputSkill.index;
                                if (index === -1 || !isEquivalent(tempInputObject,tempArray[index])) {
                                    let fullDeleted = this.state.deletedData;
                                    let tempDeleted = fullDeleted.skill;
                                    let isDeleted = tempDeleted.find(s=> s.name.toLowerCase() === this.state.inputSkill.name.toLowerCase()) !== undefined;
                                    let realIndex = index;
                                    if(isDeleted)
                                    {
                                        tempDeleted = tempDeleted.filter(s=> s.name.toLowerCase() !==this.state.inputSkill.name.toLowerCase());
                                        realIndex = tempArray.findIndex(s=>s.name.toLowerCase() === this.state.inputSkill.name.toLowerCase());
                                        fullDeleted.skill = tempDeleted;
                                    }
                                    let tempFront = tempArray;
                                    let tempBack = [];
                                    if(realIndex !== -1)
                                    {
                                        tempFront = tempArray.slice(0,realIndex);
                                        tempBack = tempArray.slice(realIndex+1,tempArray.length);
                                    }
                                    tempFront.push(tempInputObject);
                                    let temp = tempFront.concat(tempBack);
                                    fullTemp.skill = temp;
                                    this.setState({
                                        profileDataEdit: fullTemp,
                                        deletedData: fullDeleted
                                    })
                                }
                                this.modal.toggleModal(false);
                            }}
                        >
                            <Text style={{color:COLORS.WHITE}}>Confirm</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{width:"50%",height:"100%", backgroundColor:COLORS.LIGHT_GREY,alignItems:"center",justifyContent:"center"}}
                            onPress={()=>{
                                this.modal.toggleModal(false);
                            }}>
                            <Text style={{color:COLORS.BLACK}}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }else{
            return null;
        }
    }

    render(){
        return (
            <RootView>
                <Header title={"Edit Profile"} additionalAction={this.confirmAction()}/>
                <ScrollView
                    onLayout={event => {
                        const layout = event.nativeEvent.layout;
                        this.setState({
                            scrollViewOffset: layout.y
                        });
                    }}
                    onScroll={ e => {
                        this.setState({
                            scrollOffset: e.nativeEvent.contentOffset.y
                        })
                    }}
                >
                    <View
                        style={{flex:1,alignItems:"center",paddingVertical:10}}
                        onLayout={event => {
                            const layout = event.nativeEvent.layout;
                            this.setState({
                                childScrollViewOffset: layout.y
                            });
                        }}
                    >
                        <TouchableOpacity
                            style={{width: width*0.32,height:width*0.32,borderRadius:width*0.16,borderWidth:(this.state.profilePhotoEdit !== null)?0.5:0,borderColor:COLORS.RED,alignItems:"center",justifyContent:"center"}}
                            onPress={()=>{
                                this.openImagePicker();
                            }}
                        >
                            <View style={{width: width*0.3,height:width*0.3,borderRadius:width*0.15,borderWidth:0.5,borderColor:COLORS.GREY}}>
                                <Image style={{width: "100%", height: "100%",borderRadius:width * 0.15}}
                                       source={{uri: this.state.profilePhotoEdit!==null?this.state.profilePhotoEdit:ProfileStore.profilePhoto}}/>
                                <View style={{position:"absolute",right:0,bottom:0,width:30,borderRadius:15,height:30,alignItems:"center",justifyContent:"center",backgroundColor:COLORS.WHITE,borderWidth:0.5,borderColor:COLORS.GREY,opacity:1}}>
                                    <Icon size={15} color={COLORS.BLACK} name={"camera"}/>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View
                            style={{width:"95%",paddingHorizontal:20,paddingBottom:20,paddingTop:10,marginVertical:10,borderRadius:20,borderWidth:1,borderColor:COLORS.GREY}}
                            onLayout={(event)=>{
                                const layout = event.nativeEvent.layout;
                                this.setState({
                                    profileViewOffset: layout.y
                                });
                            }}
                        >
                            <Text style={{fontSize:20,fontWeight:"bold",textAlign:"center"}}>Profile</Text>
                            {this.renderProfileInput()}

                            {this.state.profileDataEdit!==null && this.state.profileDataEdit.profile.find(s=>s.key === "")===undefined?<TouchableOpacity
                                onPress={()=>{
                                    /*let tempArray = this.state.profileDataEdit;
                                    tempArray[""] = "";
                                    this.setState({
                                        profileDataEdit:tempArray
                                    });*/
                                    this.openInput({key:"",value:""},-1,"profile");
                                }}
                                style={{
                                    width:"100%",
                                    flexDirection:"row",
                                    borderRadius:10,
                                    borderColor:COLORS.BLUE,
                                    borderWidth:1,
                                    padding:10,
                                    alignItems:"center",
                                    marginTop:15
                                }}
                            >
                                <View style={{width:40,height:40,borderRadius:20,borderColor:COLORS.BLUE,borderWidth:5,alignItems:"center",justifyContent:"center",marginRight:10}}>
                                    <Icon color={COLORS.BLUE} size={24} name={"plus"}/>
                                </View>
                                <Text style={{fontWeight:"bold",fontSize:18,color:COLORS.BLUE}}>
                                    Add New Profile
                                </Text>
                            </TouchableOpacity>:null}
                        </View>
                        <View
                            style={{width:"95%",paddingHorizontal:20,paddingBottom:20,paddingTop:10,marginVertical:10,borderRadius:20,borderWidth:1,borderColor:COLORS.GREY}}
                            onLayout={(event)=>{
                                const layout = event.nativeEvent.layout;
                                this.setState({
                                    workExperienceViewOffset: layout.y
                                });
                            }}
                        >
                            <Text style={{fontSize:20,fontWeight:"bold",textAlign:"center"}}>Work Experience</Text>
                            {this.renderWorkExperienceInput()}
                            {this.state.profileDataEdit!==null && this.state.profileDataEdit.work_experience.find(s=>s.company === "")===undefined?<TouchableOpacity
                                onPress={()=>{
                                    /*let tempArray = this.state.profileDataEdit;
                                    tempArray[""] = "";
                                    this.setState({
                                        profileDataEdit:tempArray
                                    });*/
                                    this.openInput({
                                        company:"",
                                        location:"",
                                        start_date:new Date().valueOf(),
                                        end_date:new Date().valueOf(),
                                        description:"",
                                    },-1,"work");
                                }}
                                style={{
                                    width:"100%",
                                    flexDirection:"row",
                                    borderRadius:10,
                                    borderColor:COLORS.BLUE,
                                    borderWidth:1,
                                    padding:10,
                                    alignItems:"center",
                                    marginTop:15
                                }}
                            >
                                <View style={{width:40,height:40,borderRadius:20,borderColor:COLORS.BLUE,borderWidth:5,alignItems:"center",justifyContent:"center",marginRight:10}}>
                                    <Icon color={COLORS.BLUE} size={24} name={"plus"}/>
                                </View>
                                <Text style={{fontWeight:"bold",fontSize:18,color:COLORS.BLUE}}>
                                    Add New Work Experience
                                </Text>
                            </TouchableOpacity>:null}
                        </View>
                        <View
                            style={{width:"95%",paddingHorizontal:20,paddingBottom:20,paddingTop:10,marginVertical:10,borderRadius:20,borderWidth:1,borderColor:COLORS.GREY}}
                            onLayout={(event)=>{
                                const layout = event.nativeEvent.layout;
                                this.setState({
                                    educationViewOffset: layout.y
                                });
                            }}
                        >
                            <Text style={{fontSize:20,fontWeight:"bold",textAlign:"center"}}>Education</Text>
                            {this.renderEducationInput()}
                            {this.state.profileDataEdit!==null && this.state.profileDataEdit.education.find(s=>s.name === "")===undefined?<TouchableOpacity
                                onPress={()=>{
                                    /*let tempArray = this.state.profileDataEdit;
                                    tempArray[""] = "";
                                    this.setState({
                                        profileDataEdit:tempArray
                                    });*/
                                    this.openInput({
                                        name:"",
                                        degree:"",
                                        field:"",
                                        start_year:new Date().valueOf(),
                                        end_year:new Date().valueOf(),
                                        grade:"",
                                    },-1,"education");
                                }}
                                style={{
                                    width:"100%",
                                    flexDirection:"row",
                                    borderRadius:10,
                                    borderColor:COLORS.BLUE,
                                    borderWidth:1,
                                    padding:10,
                                    alignItems:"center",
                                    marginTop:15
                                }}
                            >
                                <View style={{width:40,height:40,borderRadius:20,borderColor:COLORS.BLUE,borderWidth:5,alignItems:"center",justifyContent:"center",marginRight:10}}>
                                    <Icon color={COLORS.BLUE} size={24} name={"plus"}/>
                                </View>
                                <Text style={{fontWeight:"bold",fontSize:18,color:COLORS.BLUE}}>
                                    Add New Education
                                </Text>
                            </TouchableOpacity>:null}
                        </View>
                        <View
                            style={{width:"95%",paddingHorizontal:20,paddingBottom:20,paddingTop:10,marginVertical:10,borderRadius:20,borderWidth:1,borderColor:COLORS.GREY}}
                            onLayout={(event)=>{
                                const layout = event.nativeEvent.layout;
                                this.setState({
                                    skillViewOffset: layout.y
                                });
                            }}
                        >
                            <Text style={{fontSize:20,fontWeight:"bold",textAlign:"center"}}>Skills</Text>
                            {this.renderSkillInput()}
                            {this.state.profileDataEdit!==null && this.state.profileDataEdit.skill.find(s=>s.name === "")===undefined?<TouchableOpacity
                                onPress={()=>{
                                    /*let tempArray = this.state.profileDataEdit;
                                    tempArray[""] = "";
                                    this.setState({
                                        profileDataEdit:tempArray
                                    });*/
                                    this.openInput({
                                        name:"",
                                        percent:0,
                                        defaultPercent: 0,
                                        imageURL:""
                                    },-1,"skill");
                                }}
                                style={{
                                    width:"100%",
                                    flexDirection:"row",
                                    borderRadius:10,
                                    borderColor:COLORS.BLUE,
                                    borderWidth:1,
                                    padding:10,
                                    alignItems:"center",
                                    marginTop:15
                                }}
                            >
                                <View style={{width:40,height:40,borderRadius:20,borderColor:COLORS.BLUE,borderWidth:5,alignItems:"center",justifyContent:"center",marginRight:10}}>
                                    <Icon color={COLORS.BLUE} size={24} name={"plus"}/>
                                </View>
                                <Text style={{fontWeight:"bold",fontSize:18,color:COLORS.BLUE}}>
                                    Add New Skill
                                </Text>
                            </TouchableOpacity>:null}
                        </View>
                        {this.state.deletedData.profile.length > 0 || this.state.deletedData.work_experience.length > 0 || this.state.deletedData.education.length >0 ?
                            <View style={{
                                width: "100%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "center"
                            }}>
                                <Text>
                                    There is <Text
                                    style={{color: COLORS.RED}}>{this.state.deletedData.profile.length + this.state.deletedData.work_experience.length + this.state.deletedData.education.length}</Text> deleted items
                                </Text>
                                <TouchableOpacity
                                    style={{
                                        padding: 5,
                                        marginLeft: 5,
                                        borderRadius: 5,
                                        borderWidth: 1,
                                        borderColor: COLORS.GREY
                                    }}
                                    onPress={() => {
                                        this.setState({
                                            deleteCollapse: !this.state.deleteCollapse
                                        })
                                    }}
                                >
                                    <Text>
                                        {this.state.deleteCollapse ? "Show Deleted" : "Hide Deleted"}
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={{
                                        padding: 5,
                                        marginLeft: 5,
                                        borderRadius: 5,
                                        borderWidth: 1,
                                        borderColor: COLORS.GREY
                                    }}
                                    onPress={() => {

                                        let tempFullArray = this.state.deletedData;
                                        let editObjectArray = this.state.profileDataEdit;

                                        tempFullArray.profile.map((item,index)=>{
                                            let tempObject = item;
                                            let indexObject = tempObject.index;
                                            delete tempObject.index;
                                            editObjectArray.profile.splice(indexObject, 0,tempObject);
                                        });
                                        tempFullArray.profile = [];
                                        tempFullArray.work_experience.map((item,index)=>{
                                            let tempObject = item;
                                            let indexObject = tempObject.index;
                                            delete tempObject.index;
                                            editObjectArray.work_experience.splice(indexObject, 0,tempObject);
                                        });
                                        tempFullArray.work_experience = [];
                                        tempFullArray.education.map((item,index)=>{
                                            let tempObject = item;
                                            let indexObject = tempObject.index;
                                            delete tempObject.index;
                                            editObjectArray.education.splice(indexObject, 0,tempObject);
                                        });
                                        tempFullArray.education = [];
                                        this.setState({
                                            deletedData: tempFullArray,
                                            profileDataEdit: editObjectArray
                                        });
                                    }}
                                >
                                    <Text>
                                        Undo All
                                    </Text>
                                </TouchableOpacity>
                            </View> : null}
                        <View style={{width:"100%"}}>
                            {(this.state.deletedData.profile.length > 0 || this.state.deletedData.education.length > 0 || this.state.deletedData.work_experience.length > 0) && !this.state.deleteCollapse ?
                                <View style={{width: "100%", alignItems: "center"}}>
                                    <View style={{
                                        width: "95%",
                                        paddingHorizontal: 20,
                                        paddingBottom: 20,
                                        paddingTop: 10,
                                        marginVertical: 10,
                                        borderRadius: 20,
                                        borderWidth: 1,
                                        borderColor: COLORS.GREY
                                    }}>
                                        <Text style={{fontSize: 20, fontWeight: "bold", textAlign: "center"}}>Deleted List</Text>
                                        {this.state.deletedData.profile.map((item, index) =>{
                                            return (
                                                <View style={{flex: 1, borderBottomWidth: 1,}} key={index}>
                                                    <View
                                                        style={{
                                                            flex: 1,
                                                            marginVertical: 10,
                                                            justifyContent: "center",
                                                            flexDirection: "row"
                                                        }}
                                                    >
                                                        <View style={{flex: 1}}>
                                                            <Text style={{
                                                                fontWeight: "bold",
                                                                fontSize: 16,
                                                                color: COLORS.BLACK
                                                            }}>
                                                                {item.key === "" ? "--Empty--" : Capitalized(UnderLineToSpace(item.key))}
                                                            </Text>
                                                            <Text style={{fontSize: 12, color: COLORS.BLACK}}>
                                                                {item.value === "" ? "--Empty--" : (item.key.toLowerCase().indexOf("date") !== -1 ? moment(item.value).format("DD-MM-YYYY") :item.value)}
                                                            </Text>
                                                        </View>
                                                        <TouchableOpacity
                                                            style={{height: "100%", justifyContent: "center"}}
                                                            onPress={() => {
                                                                let tempFullArray = this.state.deletedData;
                                                                let tempArray = tempFullArray.profile;
                                                                let tempObject = item;
                                                                let indexObject = tempObject.index;
                                                                delete tempObject.index;
                                                                let tempFront = tempArray.slice(0, index);
                                                                let tempBack = tempArray.slice(index + 1, tempArray.length);
                                                                tempFullArray.profile = tempFront.concat(tempBack);
                                                                let editObjectArray = this.state.profileDataEdit;
                                                                editObjectArray.profile.splice(indexObject, 0,tempObject);
                                                                this.setState({
                                                                    deletedData: tempFullArray,
                                                                    profileDataEdit: editObjectArray
                                                                });
                                                            }}
                                                        >
                                                            <Icon size={25} color={COLORS.BLACK} name={"arrow-up"}/>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            )
                                        })}

                                        {this.state.deletedData.work_experience.map((item, index) =>
                                            <View style={{flex: 1, borderBottomWidth: 1,}} key={index}>
                                                <View
                                                    style={{
                                                        flex: 1,
                                                        marginVertical: 10,
                                                        justifyContent: "center",
                                                        flexDirection: "row"
                                                    }}
                                                >
                                                    <View style={{flex:1,paddingRight:20}}>
                                                        <Text style={{fontWeight:"bold"}}>
                                                            {item.company === ""?"--Empty--":item.company}
                                                        </Text>
                                                        <Text style={{fontWeight:"bold"}}>
                                                            {moment(item.start_date).format("MMM YYYY")} - {item.end_date === "-"?"Present":moment(item.end_date).format("MMM YYYY")}
                                                        </Text>
                                                        <Text>
                                                            {item.description === ""?"--Empty--":item.description}
                                                        </Text>
                                                        <Text numberOfLines={1} ellipsizeMode={"tail"}>
                                                            {item.location === ""?"--Empty--":item.location}
                                                        </Text>
                                                    </View>
                                                    <TouchableOpacity
                                                        style={{height: "100%", justifyContent: "center"}}
                                                        onPress={() => {
                                                            let tempFullArray = this.state.deletedData;
                                                            let tempArray = tempFullArray.work_experience;
                                                            let tempObject = item;
                                                            let indexObject = tempObject.index;
                                                            delete tempObject.index;
                                                            let tempFront = tempArray.slice(0, index);
                                                            let tempBack = tempArray.slice(index + 1, tempArray.length);
                                                            tempFullArray.work_experience = tempFront.concat(tempBack);
                                                            let editObjectArray = this.state.profileDataEdit;
                                                            editObjectArray.work_experience.splice(indexObject, 0,tempObject);
                                                            this.setState({
                                                                deletedData: tempFullArray,
                                                                profileDataEdit: editObjectArray
                                                            });
                                                        }}
                                                    >
                                                        <Icon size={25} color={COLORS.BLACK} name={"arrow-up"}/>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        )}
                                        {this.state.deletedData.education.map((item, index) =>
                                            <View style={{flex: 1, borderBottomWidth: 1,}} key={index}>
                                                <View
                                                    style={{
                                                        flex: 1,
                                                        marginVertical: 10,
                                                        justifyContent: "center",
                                                        flexDirection: "row"
                                                    }}
                                                >
                                                    <View style={{flex:1,paddingRight:20}}>

                                                        <Text style={{fontWeight:"bold"}}>
                                                            {item.name === ""?"--Empty--":item.name}
                                                        </Text>
                                                        <Text style={{fontWeight:"bold"}}>
                                                            {moment(item.start_year).format("YYYY")} - {item.end_year === "-"?"Present":moment(item.end_year).format("YYYY")}
                                                        </Text>
                                                        <Text>
                                                            {item.degree === ""?"--Empty--":item.degree}
                                                        </Text>
                                                        <Text numberOfLines={1} ellipsizeMode={"tail"}>
                                                            {item.field === ""?"--Empty--":item.field}
                                                        </Text>
                                                        <Text numberOfLines={1} ellipsizeMode={"tail"}>
                                                            {item.grade === ""?"--Empty--":item.grade+" GPA"}
                                                        </Text>
                                                    </View>
                                                    <TouchableOpacity
                                                        style={{height: "100%", justifyContent: "center"}}
                                                        onPress={() => {
                                                            let tempFullArray = this.state.deletedData;
                                                            let tempArray = tempFullArray.education;
                                                            let tempObject = item;
                                                            let indexObject = tempObject.index;
                                                            delete tempObject.index;
                                                            let tempFront = tempArray.slice(0, index);
                                                            let tempBack = tempArray.slice(index + 1, tempArray.length);
                                                            tempFullArray.education = tempFront.concat(tempBack);
                                                            let editObjectArray = this.state.profileDataEdit;
                                                            editObjectArray.education.splice(indexObject, 0,tempObject);
                                                            this.setState({
                                                                deletedData: tempFullArray,
                                                                profileDataEdit: editObjectArray
                                                            });
                                                        }}
                                                    >
                                                        <Icon size={25} color={COLORS.BLACK} name={"arrow-up"}/>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        )}
                                    </View>
                                </View> : null}
                        </View>
                        <TouchableOpacity
                            style={{width:"100%",paddingBottom:10,paddingTop:5,alignItems:"center"}}
                            onPress={()=>{
                                ConfirmModal.toggleConfirmation("Do you want to sign out?",()=>{
                                    LoadingModal.toggleLoading(true);
                                    firebase.auth().signOut().then(async () => {
                                        await AsyncStorage.removeItem("SignStatus");
                                        loginStore.userData = null;
                                        LoadingModal.toggleLoading(false);
                                        RouterDirector(RouterList.DashboardPage);
                                    });
                                });
                            }}
                        >
                            <Text style={{fontWeight:"bold",fontSize:24,color:COLORS.RED}}>Log Out</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <ComponentModal ref={ref => this.modal = ref}>
                    {this.renderInputModal()}
                </ComponentModal>
            </RootView>
        )
    }
}
