
import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import {observer} from 'mobx-react';
import PropTypes  from 'prop-types';

import Swiper from 'react-native-swiper';
import {Header, Image, RootView,Icon} from "../Module";
import modalStore from "../Stores/modalStore";
import COLORS from "../Constant/Colors";
import * as RouterList from "../Routers/routerList";
import {RouterDirector} from "../Routers/routerManager";
import LottieView from 'lottie-react-native';
import {CHECK_ANIM} from "../Assets";

@observer
export default class SuccessPage extends Component{

    static defaultProps = {
        successText: "Congratulation",
        successSentence: "Your Profile has Been Updated!",
    };

    static propTypes = {
        successText: PropTypes.any,
        successSentence: PropTypes.any
    };

    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.check.play();
    }


    render(){
        return (
            <RootView>
                <View style={{flex:1,alignItems:"center"}}>
                    <View style={{flex:1,alignItems:"center",justifyContent:"center",padding:20}}>
                        {/*<View style={{width:400,height:400,marginBottom:10,}}>
                            <Icon name={"check"} color={COLORS.GREEN} type={"material"} size={60}/>
                        </View>*/}
                        <LottieView
                            style={{width:250,height:250,marginBottom:-70}}
                            ref={animation => {
                                this.check = animation;
                            }}
                            loop={false}
                            source={CHECK_ANIM}
                        />
                        <Text style={{textAlign:"center",fontSize:20,fontWeight:"bold",marginBottom:10}}>{this.props.successText}</Text>
                        <Text style={{textAlign:"center",fontSize:14}}>{this.props.successSentence}</Text>
                    </View>
                    <TouchableOpacity
                        style={{paddingHorizontal:20,paddingVertical:10,borderRadius:30,marginBottom:40,backgroundColor:COLORS.BLUE}}
                        onPress={()=>{
                            RouterDirector(RouterList.DashboardPage);
                        }}
                    >
                        <Text style={{color:COLORS.WHITE}}>
                            Go Back
                        </Text>
                    </TouchableOpacity>
                </View>
            </RootView>
        )
    }
}