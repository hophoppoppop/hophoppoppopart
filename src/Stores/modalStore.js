import {observable} from 'mobx';

export default modalStore = observable({
    //initial state for store
    imageViewer:{
        visible: false,
        url: null,
    },
    confirmModal:{
        visible: false,
        description: "",
        acceptText: "Confirm",
        declineText: "Cancel",
        acceptAction: ()=>{

        },
        declineAction: ()=>{

        }
    },
    pickerModal:{
        visible: false,
        title:"",
        data: [],
        pickAction: ()=>{

        }
    },
    loadingModal:{
        visible: false,
    },
    datePickerModal:{
        visible: false,
        acceptAction: ()=>{

        },
        declineAction: ()=>{

        },
        selectedYearIndex: 0,
        selectedMonthIndex: 0,
        yearOnly: false,
        monthOnly: false
    }
});