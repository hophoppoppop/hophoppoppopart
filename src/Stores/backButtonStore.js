import {observable} from 'mobx';
import * as RouterList from '../Routers/routerList';

export default backButtonStore = observable({
    //initial state for store
    backButton:{
        routerStack: [],
        disabledBackRoutes: [
        ],
        closeBackRoutes: [
            RouterList.DashboardPage
        ],
    },
    exitToastShow:false,
});