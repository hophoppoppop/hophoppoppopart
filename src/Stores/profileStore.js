import {observable} from 'mobx';

export default profileStore = observable({
    //initial state for store
    profileData: null,
    profilePhoto: null,
    lastUpdated: 0,
});