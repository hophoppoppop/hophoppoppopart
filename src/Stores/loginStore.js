import {observable} from 'mobx';

export default loginStore = observable({
    //initial state for store
    userData:{
        phoneNumber: null,
        isAnonymous: false
    }
});