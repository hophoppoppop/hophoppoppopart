import { NavigationActions, StackActions  } from 'react-navigation';
import type { NavigationParams, NavigationRoute } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';

let _container; // eslint-disable-line

function setContainer(container: Object) {
    _container = container;
}

function reset(routeName: string, params?: NavigationParams) {
    if(routeName !== currentScene())
    {
        _container.dispatch(
            StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({
                        type: 'Navigation/NAVIGATE',
                        routeName,
                        params,
                    }),
                ],
            }),
        );
    }
}

function navigate(routeName: string, params) {
    let temp = NavigationActions.navigate({
        type: 'Navigation/NAVIGATE',
        routeName,
        params:params,
    });
    if(params !== undefined && params.navigationReplace !== undefined && params.navigationReplace !== null && params.navigationReplace)
    {
        temp = StackActions.replace({
                routeName,
                params:params,
            })
    }
    if(routeName !== currentScene()) {
        _container.dispatch(
            temp
        );
    }
}

function pop(params: NavigationParams, routeName?: string){
    _container.dispatch(
        NavigationActions.back({
            type: 'Navigation/NAVIGATE',
            key:routeName,
            params:params,
        })
    );
}

function navigateDeep(actions: { routeName: string, params?: NavigationParams }[]) {
    _container.dispatch(
        actions.reduceRight(
            (prevAction, action): any =>
                NavigationActions.navigate({
                    type: 'Navigation/NAVIGATE',
                    routeName: action.routeName,
                    params: action.params,
                    action: prevAction,
                }),
            undefined,
        ),
    );
}

function currentScene(): NavigationRoute | null {
    if (!_container || !_container.state.nav) {
        return null;
    }

    return _container.state.nav.routes[_container.state.nav.index].routeName || null;
}

function getScenewithIndex(index){
    if (!_container || !_container.state.nav || _container.state.nav.index < index) {
        return null;
    }

    return _container.state.nav.routes[_container.state.nav.index-index] || null;
}

function replace(routeName: string, params?: NavigationParams) {
    _container.dispatch(
        StackActions.replace({
            routeName,
            params,
        }),
    );
}

function openDrawer(){
    _container.dispatch(
        DrawerActions.openDrawer()
    );
}

function closeDrawer(){
    _container.dispatch(
        DrawerActions.closeDrawer()
    );
}

export default {
    setContainer,
    navigateDeep,
    navigate,
    reset,
    currentScene,
    getScenewithIndex,
    pop,
    replace,
    openDrawer,
    closeDrawer
};