// /LOGIN
export const DashboardPage = 'DashboardPage';
export const LoginPage = 'LoginPage';
export const OtpPage = 'OtpPage';
export const EditPage = 'EditPage';
export const ImageViewerPage = 'ImageViewerPage';
export const CameraPage = 'CameraPage';
export const SuccessPage = 'SuccessPage';
export const TestPage = 'TestPage';
