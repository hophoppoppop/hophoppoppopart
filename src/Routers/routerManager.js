import React, {Component} from 'react';
import {ScrollView, View, Dimensions, Platform, ToastAndroid, Text, BackHandler, Linking} from 'react-native';
import StackViewStyleInterpolator from 'react-navigation-stack/lib/module/views/StackView/StackViewStyleInterpolator';
import {createAppContainer } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import * as RouterList from './routerList';

import NavigatorService from "./navigator";

import DashboardPage from "../Pages/DashboardPage";
import LoginPage from "../Pages/LoginPage";
import ImageViewerPage from "../Pages/ImageViewerPage";
import EditPage from "../Pages/EditPage";
import OtpPage from "../Pages/OtpPage";
import CameraPage from "../Pages/CameraPage";
import SuccessPage from "../Pages/SuccessPage";
import TestPage from "../Pages/TestPage";
import backButtonStore from "../Stores/backButtonStore";

//Mengubah pemanggilan this.props.navigation.params jadi this.props
const mapNavigationStateParamsToProps = (ScreenComponent) => {
    return class extends Component {
        static navigationOptions = ScreenComponent.navigationOptions;
        render() {
            const { params } = this.props.navigation.state;
            return <ScreenComponent {...this.props} {...params} />
        }
    }
};

const Router = createAppContainer(createStackNavigator({
        DashboardPage:{screen: mapNavigationStateParamsToProps(DashboardPage)},
        LoginPage:{screen: mapNavigationStateParamsToProps(LoginPage)},
        ImageViewerPage: {screen: mapNavigationStateParamsToProps(ImageViewerPage)},
        EditPage: {screen: mapNavigationStateParamsToProps(EditPage)},
        OtpPage: {screen: mapNavigationStateParamsToProps(OtpPage)},
        CameraPage: {screen: mapNavigationStateParamsToProps(CameraPage)},
        SuccessPage: {screen: mapNavigationStateParamsToProps(SuccessPage)},
        TestPage: {screen: mapNavigationStateParamsToProps(TestPage)}
    },
    {
        initialRouteName: "DashboardPage",
        headerMode: 'none',
        transitionConfig: () => ({ screenInterpolator: StackViewStyleInterpolator.forHorizontal }),
    },)
);


const prevGetStateForActionHomeStack = Router.router.getStateForAction;
Router.router.getStateForAction = (action, state) => {
    if (state && action.type === 'ReplaceCurrentScreen') {
        const routes = state.routes.slice(0, state.routes.length - 1);
        routes.push(action);
        return {
            ...state,
            routes,
            index: routes.length - 1,
        };
    }
    return prevGetStateForActionHomeStack(action, state);
};

export default class RouterManager extends Component{

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router ref={navigatorRef => {
                NavigatorService.setContainer(navigatorRef);
            }}/>
        )
    }
};

//export const Drawer =

export const RouterDirector = (Key, Props) => {
    if(backButtonStore.backButton.routerStack[backButtonStore.backButton.routerStack.length-1] !== Key)
    {
        backButtonStore.backButton.routerStack.push({
            Key:Key
        });
        switch(Key){
            case RouterList.DashboardPage:
                NavigatorService.reset(Key,Props);
                break;
            case RouterList.LoginPage:
                NavigatorService.navigate(Key,Props);
                break;
            case RouterList.ImageViewerPage:
                NavigatorService.navigate(Key,Props);
                break;
            case RouterList.EditPage:
                NavigatorService.navigate(Key,Props);
                break;
            case RouterList.OtpPage:
                NavigatorService.navigate(Key,Props);
                break;
            case RouterList.CameraPage:
                NavigatorService.navigate(Key,Props);
                break;
            case RouterList.SuccessPage:
                NavigatorService.navigate(Key,Props);
                break;
            case RouterList.TestPage:
                NavigatorService.reset(Key,Props);
                break;
        }
    }
};

export const PagePop = () => {
    NavigatorService.pop();
};

export const OpenDrawer = () => {
    NavigatorService.openDrawer();
};

export const CloseDrawer = () => {
    NavigatorService.closeDrawer();
};

export const PagePopTo = (key,props) => {
    NavigatorService.pop(props,key);
};

export const Replacing = (key,props) => {
    NavigatorService.replace(key,props);
};

export const GetCurrentScene = () => {
    return NavigatorService.currentScene();
};


export const PagePopRefresh = (props) => {
    NavigatorService.pop(props);
};